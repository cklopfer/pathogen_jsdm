---
title: "Fitted Crossval Analysis"
output: html_document
---

```{r setup, include=FALSE}
library(dplyr)
library(ggplot2)
library(igraph)
source(file.path("..", "scripts", "post_processing.R"), echo = F)
source(file.path("..", "scripts", "visualisations.R"), echo = F)
```

# Data Import 

```{r dic_scores}
dic_crossval_scores <- readr::read_csv(file.path("..", "data", "dic_scores", "dic_scores_newborn_initial.csv"))

```


```{r dic_plot}
dic_crossval_scores %>% 
  ggplot(aes(x = model_type, y = dic_scores, fill = model_type)) +
  geom_boxplot()+
  scale_y_log10()
```

# Community Model 
```{r community_trace_import}
community_bayescom_chain <- import_parallel_models("community_only", "bayescomm_earlylife_5e4_")
community_corr_coef <- combine_bayescomm_corr_coef(community_bayescom_chain)
community_env_coef <- combine_bayescomm_env_coef(community_bayescom_chain)
```

### Residual Coefficients

```{r community_res_coef, fig.width=20, fig.height=80}
# They're looking good! Might just want to run them for a little longer to promote burn in. 
plot_bayescomm_corr_coef(community_corr_coef)
```
### ENV Coefficients 

```{r community_env_coef}
plot_bayescomm_env_coef(community_env_coef)
```

# ENV Model

```{r env_trace_import}
env_bayescom_chain <- import_parallel_models("env_only", "bayescomm_earlylife_5e4_")
env_corr_coef <- combine_bayescomm_corr_coef(env_bayescom_chain)
env_env_coef <- combine_bayescomm_env_coef(env_bayescom_chain)
```

### Residual Coefficients

```{r, fig.width=20, fig.height=80}
# They're looking good! Might just want to run them for a little longer to promote burn in. 
plot_bayescomm_corr_coef(env_corr_coef)
```

### ENV Coefficients 

```{r community_env_coef, fig.height= 12, fig.width = 12}
plot_bayescomm_env_coef(env_env_coef)
```

# Full Model 

```{r full_model_trace_import}
full_bayescom_chain <- import_parallel_models("full_model", "bayescomm_earlylife_5e4_")
full_corr_coef <- combine_bayescomm_corr_coef(full_bayescom_chain)
full_env_coef <- combine_bayescomm_env_coef(full_bayescom_chain)
```

### Residual Coefficients

```{r, fig.width=20, fig.height=80}
# They're looking good! Might just want to run them for a little longer to promote burn in. 
plot_bayescomm_corr_coef(full_corr_coef)
```
### ENV Coefficients 

```{r community_env_coef, fig.width = 20, fig.height = 10}
plot_bayescomm_env_coef(full_env_coef)
```

# Null Chain

```{r null_chain}
null_bayescom_chain <- import_parallel_models("null_model", "bayescomm_earlylife_5e4_")
```

# Generate confidence intervals 

```{r residual_cis}
community_residuals <- residual_cis(community_bayescom_chain[[1]])
env_residuals <- residual_cis(env_bayescom_chain[[1]])
full_residuals <- residual_cis(full_bayescom_chain[[1]])
```

```{r shared_env_response}
# SHould we consider finding the mean? We want to the median if the distribution is skewed. 
env_env_response <- shared_env_response(env_bayescom_chain[[1]])
full_env_response <- shared_env_response(full_bayescom_chain[[1]])
```

```{r fig.height=10, fig.wdith = 10}
# Save residual Plots to file. 
png(filename = file.path("..","figures", "bayescomm_residuals", "community_residual.png"), res = 720, width = 12, height = 12, units = "in")
community_residual <- plot_path_corr(community_residuals[['mean_corr_sig']])
dev.off()

png(filename = file.path("..","figures", "bayescomm_residuals", "env_residual.png"), res = 720, width = 12, height = 12, units = "in")
env_residual <- plot_path_corr(env_residuals[['mean_corr_sig']])
dev.off()

png(filename = file.path("..","figures", "bayescomm_residuals", "full_residual.png"), res = 720, width = 12, height = 12, units = "in")
full_residual <- plot_path_corr(full_residuals[['mean_corr_sig']])
dev.off()

# there's a problem with Tanzania mean, can it be larger than 1? 
plot_path_corr(full_residuals[['mean_corr_sig']])
```

```{r env_plots, fig.height=10, fig.width = 14}
my_corr_plot(env_env_response[['stool_type']][['median']])
my_corr_plot(env_env_response[['Tanzania']][['median']])
my_corr_plot(full_env_response$all$sig_mean)
my_corr_plot(full_env_response[['stool_type']][['median']])
my_corr_plot(full_env_response[['Tanzania']][['median']])
```

```{r, all_env_response_early_life}

```


# Deviance Information Criterion

```{r dic_metrics_all_models}
# Might need to set memory limit to 20.000
BayesComm::DIC(community_bayescom_chain[[1]])
BayesComm::DIC(null_bayescom_chain[[1]])
BayesComm::DIC(env_bayescom_chain[[1]])
BayesComm::DIC(full_bayescom_chain[[1]])
```

The DIC indicates that the most information is explained by the full model, since it has the lowest DIC. 

TODO: Read the original Bayescomm package for a more in-depth explanation for the Bayescomm paper. 


# Shared ENV Response 

I Need my own function to find the shared env response. BAyesComm package does not have this implementation, so I need to build my own. There's an equation in Pollock 2015 that I'll use. 

**TODO** I should really do a forward stepwise regression to determine the appropriate response variables 


```{r calc_env_reponse}
full_array <- find_shared_env_response(full_bayescom_chain[[1]])
colnames(env_bayescom_chain[[1]]$trace$B[[1]])
```

NotE: Th posteiors are wide and flat, which is leaving me to believe that there is not enough data to overcome with wide flat priors provided in the model. Perhaps, I can input some tighter priors aread of time to overcome this in the future? WISH I HAD MORE TIME!!!!

```{r plot_env_reponse_all, fig.height=10, fig.width=10}
plot_env_corr(full_array[['sig_m']])
plot_all_corrplots(full_env_reponse_all)
plot_all_corrplots(env_response_all)
```

```{r, fig.height=10, fig.width=10}
plot_env_corr(full_array[['sig_m']])
plot_all_corrplots(full_env_reponse_all)
plot_all_corrplots(env_response_all)
```


```{r, fig.width=10, fig.height=10}
# There's an inner term that we'll calculate to help find the shared response to the envirnment
# test_array <- find_shared_response(env_bayescom_chain[[1]])
plot_env_corr(env_array[['sig_m']])
plot_env_corr(env_array[['mean_m']])
plot_env_corr(full_array[['sig_m']])
plot_env_corr(env_array_subset[['sig_m']])
```

```{r, shred_responce_trace, fig.width = 20, fig.height = 30}

full_array[[7]] %>% tidyr::pivot_longer(-c("iter", 'pathogen_2'), 
                                        names_to = "pathogen_1", 
                                        values_to = "response") %>% 
  filter(!is.na(response)) %>%
  ggplot()+
  geom_histogram(aes(x = response), bins = 50)+
  scale_x_continuous(breaks = c(-1, 0, 1))+
  theme(panel.grid.minor.x =  element_blank())+
  facet_grid(pathogen_1~pathogen_2)


env_array[[7]] %>% 
  tidyr::pivot_longer(-c("iter", 'pathogen_2'), 
                      names_to = "pathogen_1", 
                      values_to = "response") %>% 
  filter(!is.na(response)) %>%
  ggplot()+
  geom_histogram(aes(x = response), bins = 50)+
  scale_x_continuous(breaks = c(-1, 0, 1))+
  theme(panel.grid.minor.x =  element_blank())+
  facet_grid(pathogen_1~pathogen_2)

```

 Porbably should test that the env coefficient is working with an example. 


## Network Representations
```{r fig.height=8, fig.width=10}
# Save the Residual Circle Nets to file
png(filename = file.path("..", "figures", "bayescomm_residuals", "community_only_net.png"), res = 300, 
    width = 10,
    height = 10, units = "in")
community_residual_net <- plot_circle_net(community_residuals[['mean_corr_sig']], 
                                          plot_title = "", 
                                          type = "biotic")
print(community_residual_net)
dev.off()


png(filename = file.path("..", "figures", "bayescomm_residuals", "full_model_net.png"), res = 300, 
    width = 10,
    height = 10, units = "in")
par(mar = c(1, 0, 0, 0))
full_residual_net <- plot_circle_net(full_residuals[['mean_corr_sig']], 
                                          plot_title = "", 
                                          type = "biotic")
print(full_residual_net)
dev.off()



save_env_models <- function(matrices, parent_dir){
  title_relabel <- c(
    "intercept" = "Intercept", 
    "1 month" = "1 Month",
    "3 month" = "3 Month", 
    "wet_season" = "Wet Season", 
    "stool_type" = "Diarrheal Stool", 
    "South Africa" = "South Africa", 
    "Pakistan" = "Pakistan", 
    "Peru" = "Peru", 
    "Nepal" = "Nepal", 
    "Bangladesh" = "Bangladesh", 
    "India" = "India", 
    "Tanzania"= "Tanzania", 
    "improved_water" = "Improved Water Source", 
    "improved_toilet" = "Improved Toilet Sanitation", 
    "breast_feeding_only" = "Exclusively Breast Freeding",
    "maternal_education" = "Maternal Education"
    
  )
  for(v in names(matrices)){
    if(v != "all"){
      png(filename = file.path(parent_dir, 
                               paste0(v, ".png")), 
                               res = 720, 
                               width = 14, 
                               height = 10, units = "in")
      temp <- my_corr_plot(env_env_response[[v]][['median']], my_title = title_relabel[v], type = "environment")
      print(temp)
      dev.off()
    }
  }
  # png(filename = file.path("..","figures", "bayescomm_residuals", "community_residual.png"), res = 720, width = 12, height = 12, units = "in")
  # community_residual <- plot_path_corr(community_residuals[['mean_corr_sig']])
  # dev.off()
}
save_env_models(full_env_response, file.path("..", "figures", "bayescomm_env", "full_model"))

save_env_models(env_env_response, file.path("..", "figures", "bayescomm_env", "env_only"))


## Save full shared response 
png(filename = file.path("..", "figures", "bayescomm_env", "env_only", "env_all_mean_corrplot.png"), 
                                   res = 720, 
                               width = 12, 
                               height = 10, units = "in")
plot_path_corr(env_env_response[['all']][['sig_median']], my_title = "Total ENV Response", type = "environment")
# print(env_all_plot)
dev.off()

## Save full shared response 
png(filename = file.path("..", "figures", "bayescomm_env", "full_model", "full_all_mean_corrplot.png"), 
                                   res = 720, 
                               width = 12, 
                               height = 10, units = "in")
plot_path_corr(full_env_response[['all']][['sig_median']], my_title = "Full Model Environment Response", type = "environment")
# print(env_all_plot)
dev.off()


## Save full shared response 
png(filename = file.path("..", "figures", "bayescomm_env", "env_only", "env_all_mean_net.png"), 
                                   res = 720, 
                               width = 12, 
                               height = 10, units = "in")
env_response_net <- plot_circle_net(env_env_response[['all']][['sig_median']], plot_title = "Envirnment Only Environment Response", type = "environment")
print(env_response_net)
dev.off()

## Save full shared response 
png(filename = file.path("..", "figures", "bayescomm_env", "full_model", "full_all_mean_net.png"), 
                                   res = 720, 
                               width = 12, 
                               height = 10, units = "in")
full_env_response_net <- plot_circle_net(full_env_response[['all']][['sig_median']], 
                                        plot_title = "Full Model Environment Response", type = "environment")
print(full_env_response_net)
dev.off()

```

```{r}
melt_correlations <- function(residual_m, env_m){
  all_dfs <- list()
  all_m <- list(
    'res_mean' = residual_m[['mean_corr']],
    'res_low' = residual_m[['corr_low']],
    'res_high' = residual_m[['corr_high']],
    'env_mean' = env_m[['all']][['mean_m']],
    'env_low' = env_m[['all']][['low_m']],
    'env_high' = env_m[['all']][['high_m']] 
  )
  
  for(m in names(all_m)){
    all_m[[m]][upper.tri(all_m[[m]])] <- NA
    all_dfs[[m]] <- all_m[[m]] %>% as.data.frame() %>% 
      mutate(data_type = m, path1 = colnames(all_m[[m]])) 
  }
  
  final <- do.call(rbind, all_dfs) %>%
    tidyr::pivot_longer(colnames(residual_m[['mean_corr']]), names_to = "path2", 
                        values_to = "value") %>%
    tidyr::pivot_wider(names_from = "data_type", values_from = "value")
  
  return(final)
}

melted_corrs <- melt_correlations(full_residuals, full_env_response)


double_corr_plot <- melted_corrs %>% 
  filter(path1 != path2) %>% 
  filter(complete.cases(.)) %>% 
  mutate(flag = case_when(
    (res_low < 0 & res_high > 0) & !(env_low < 0 & env_high > 0) ~ "Environment", 
    !(res_low < 0 & res_high > 0) & (env_low < 0 & env_high > 0) ~ "Residual (Biotic)",
    !(res_low < 0 & res_high > 0) & !(env_low < 0 & env_high > 0) ~ "Both", 
    TRUE ~ "Neither"
  )) %>% 
  ggplot(aes(x = env_mean, y = res_mean))+
  geom_errorbar(aes(ymin = res_low, ymax = res_high), alpha = 0.2)+
  geom_errorbarh(aes(xmin = env_low, xmax = env_high), alpha = 0.2)+
  geom_point(aes(fill = flag), color = "black", stroke = 0.1, shape = 21, size = 3)+
  xlim(-1, 1)+
  ylim(-1, 1)+
  scale_fill_manual(values = c("Environment" = "#63ACBE", "Both" = "#601A4A", "Neither" = "#F9F4EC", "Residual (Biotic)" = "#EE442F"))+
  labs(x = "Environment Correlation", y = "Residual Correlation", fill = "Significant in:", 
       title = "Relationship between residual and environment correlations")+
  theme(panel.background = element_blank(), 
        panel.border = element_rect(color = "black", fill = NA), 
        panel.grid.major = element_line(color = "gray"))

ggsave(filename = file.path("..", "figures", "corrplot_relationship.png"), 
       height = 7, 
       width = 9,
       units = "in",
       plot = double_corr_plot)

```
# Late Life Model

```{r chain_import}
# Memory Limits: We should clear all data inst
rm(list = setdiff(ls(), lsf.str()))

community_bayescom_chain_latelife <- import_parallel_models("community_only", "bayescomm_latelife_5e4_")
community_corr_coef_latelife <- combine_bayescomm_corr_coef(community_bayescom_chain_latelife)
community_env_coef_latelife <- combine_bayescomm_env_coef(community_bayescom_chain_latelife)

env_bayescom_chain_latelife <- import_parallel_models("env_only", "bayescomm_latelife_5e4_")
env_corr_coef_latelife <- combine_bayescomm_corr_coef(env_bayescom_chain_latelife)
env_env_coef_latelife <- combine_bayescomm_env_coef(env_bayescom_chain_latelife)

full_bayescom_chain_latelife <- import_parallel_models("full_model", "bayescomm_latelife_5e4_")
full_corr_coef_latelife <- combine_bayescomm_corr_coef(full_bayescom_chain_latelife)
full_env_coef_latelife <- combine_bayescomm_env_coef(full_bayescom_chain_latelife)

null_bayescom_chain_latelife <- import_parallel_models("null_model", "bayescomm_latelife_5e4_")
```

```{r, community_late_life_chains_residual, fig.height=80, fig.width=20}
plot_bayescomm_corr_coef(community_corr_coef_latelife)
```

 These chains look they're diverging a little bit, we might want to run things for a long time over night. 
```{r, env_late_lifechains_env, fig.width = 20, fig.height = 10}
plot_bayescomm_env_coef(env_env_coef_latelife)
```

```{r full_model_community_chains, fig.height=80, fig.width=20}
plot_bayescomm_corr_coef(full_corr_coef_latelife)
```


```{r full_model_env_chains, fig.width = 20, fig.height = 10}
plot_bayescomm_env_coef(full_env_coef_latelife)
```

```{r late_life_dic}
BayesComm::DIC(community_bayescom_chain_latelife[[1]])
BayesComm::DIC(null_bayescom_chain_latelife[[1]])
BayesComm::DIC(env_bayescom_chain_latelife[[1]])
BayesComm::DIC(full_bayescom_chain_latelife[[1]])
```

```{r late_life_devpart}
deviance_grade <- BayesComm::devpart(null_bayescom_chain_latelife[[1]], 
                                     env_bayescom_chain_latelife[[1]], 
                                     community_bayescom_chain_latelife[[1]], 
                                     full_bayescom_chain_latelife[[1]])
```

```{r latelife_residuals}
community_residuals_latelife <- residual_cis(community_bayescom_chain_latelife[[1]])
env_residuals_latelife <- residual_cis(env_bayescom_chain_latelife[[1]])
full_residuals_latelife <- residual_cis(full_bayescom_chain_latelife[[1]])
```

```{r, latelife_residuals}
# Save residual Plots to file. 
png(filename = file.path("..","figures", "bayescomm_residuals", "community_residual_latelife.png"), res = 300, width = 12, height = 12, units = "in")
community_residual <- plot_path_corr(community_residuals_latelife[['mean_corr_sig']])
dev.off()

png(filename = file.path("..","figures", "bayescomm_residuals", "env_residual_latelife.png"), res = 300, width = 12, height = 12, units = "in")
env_residual <- plot_path_corr(env_residuals_latelife[['mean_corr_sig']])
dev.off()

png(filename = file.path("..","figures", "bayescomm_residuals", "full_residual_latelife.png"), res = 300, width = 12, height = 12, units = "in")
full_residual <- plot_path_corr(full_residuals_latelife[['mean_corr_sig']])
dev.off()


# Save the Residual Circle Nets to file
png(filename = file.path("..", "figures", "bayescomm_residuals", "community_only_net_latelife.png"), res = 300, 
    width = 10,
    height = 10, units = "in")
community_residual_net_latelife <- plot_circle_net(community_residuals_latelife[['mean_corr_sig']], 
                                          plot_title = "", 
                                          type = "biotic")
print(community_residual_net_latelife)
dev.off()


png(filename = file.path("..", "figures", "bayescomm_residuals", "full_model_net_latelife.png"), res = 300, 
    width = 10,
    height = 10, units = "in")
par(mar = c(1, 0, 0, 0))
full_residual_net_latelife <- plot_circle_net(full_residuals_latelife[['mean_corr_sig']], 
                                          plot_title = "", 
                                          type = "biotic")
print(full_residual_net_latelife)
dev.off()
```

```{r latelife_env_import}
env_env_response_latelife <- shared_env_response(env_bayescom_chain_latelife[[1]])
full_env_response_latelife <- shared_env_response(full_bayescom_chain_latelife[[1]])
```

```{r latelife_env_plots}
## Save full shared response 
png(filename = file.path("..", "figures", "bayescomm_env", "env_only", "env_all_mean_corrplot_latelife.png"), 
                                   res = 300, 
                               width = 12, 
                               height = 10, units = "in")
plot_path_corr(env_env_response_latelife[['all']][['sig_mean']], my_title = "", type = "environment")
# print(env_all_plot)
dev.off()

## Save full shared response 
png(filename = file.path("..", "figures", "bayescomm_env", "full_model", "full_all_mean_corrplot_latelife.png"), 
                                   res = 300, 
                               width = 12, 
                               height = 10, units = "in")
plot_path_corr(full_env_response_latelife[['all']][['sig_mean']], my_title = "", type = "environment")
# print(env_all_plot)
dev.off()


## Save full shared response 
png(filename = file.path("..", "figures", "bayescomm_env", "env_only", "env_all_mean_net_latelife.png"), 
                                   res = 300, 
                               width = 12, 
                               height = 10, units = "in")
env_response_net <- plot_circle_net(env_env_response_latelife[['all']][['sig_mean']], plot_title = "", type = "environment")
print(env_response_net)
dev.off()

## Save full shared response 
png(filename = file.path("..", "figures", "bayescomm_env", "full_model", "full_all_mean_net_latelife.png"), 
                                   res = 300, 
                               width = 12, 
                               height = 10, units = "in")
full_env_response_net <- plot_circle_net(full_env_response_latelife[['all']][['sig_mean']], 
                                        plot_title = "Full Model Environment Response", type = "environment")
print(full_env_response_net)
dev.off()

```
```{r, fig.height= 12, fig.width=12}
for(n in names(env_env_response_latelife)){
  if(n != "all"){
    print(my_corr_plot(env_env_response_latelife[[n]]$mean, n, type = "environment"))
  }
}

my_corr_plot(env_env_response_latelife[['all']]$sig_median, "", type = "environment")
my_corr_plot(full_env_response_latelife[['all']]$sig_median, "", type = "environment")

# my_corr_plot(env_env_response_latelife[['all']]$low_m, "", type = "environment")
# my_corr_plot(env_env_response_latelife[['all']]$high_m, "", type = "environment")
# my_corr_plot(full_env_response_latelife[['all']]$mean, "", type = "environment")
# 
# 
# my_corr_plot(env_env_response[['all']]$low_m, "", type = "environment")
```


```{r}
env_env_response_latelife$all$flattened %>% as.data.frame() %>% 
  ggplot()+
  geom_density(aes(x = `st_etec*etec`), bins = 40, fill = "blue", alpha = 0.3)+
  geom_density(aes(x = `norovirus gii*sapovirus`), fill = "red", alpha = 0.3, bins = 40)+
  geom_density(aes(x = `eaec*lt_etec`), fill = "green", alpha = 0.3, bins = 40)+
  geom_vline(aes(xintercept = mean(`st_etec*etec`)), color = "red")+
  geom_vline(aes(xintercept = median(`st_etec*etec`)), color = "blue")
```



```{r corr_relationship_latelife}
melted_latelife <- melt_correlations(full_residuals_latelife, full_env_response_latelife)

melted_latelife_plot <- melted_latelife %>% 
  filter(path1 != path2) %>% 
  filter(complete.cases(.)) %>% 
  mutate(flag = case_when(
    (res_low < 0 & res_high > 0) & !(env_low < 0 & env_high > 0) ~ "Environment", 
    !(res_low < 0 & res_high > 0) & (env_low < 0 & env_high > 0) ~ "Residual (Biotic)",
    !(res_low < 0 & res_high > 0) & !(env_low < 0 & env_high > 0) ~ "Both", 
    TRUE ~ "Neither"
  )) %>% 
  ggplot(aes(x = env_mean, y = res_mean))+
  geom_errorbar(aes(ymin = res_low, ymax = res_high), alpha = 0.2)+
  geom_errorbarh(aes(xmin = env_low, xmax = env_high), alpha = 0.2)+
  geom_point(aes(fill = flag), color = "black", stroke = 0.1, shape = 21, size = 3)+
  xlim(-1, 1)+
  ylim(-1, 1)+
  scale_fill_manual(values = c("Environment" = "#63ACBE", "Both" = "#601A4A", "Neither" = "#F9F4EC", "Residual (Biotic)" = "#EE442F"))+
  labs(x = "Environment Correlation", y = "Residual Correlation", fill = "Significant in:", 
       title = "Relationship between residual and environment correlations")+
  theme(panel.background = element_blank(), 
        panel.border = element_rect(color = "black", fill = NA), 
        panel.grid.major = element_line(color = "gray"))

ggsave(filename = file.path("..", "figures", "corrplot_relationship_latelife.png"), 
       height = 7, 
       width = 9,
       units = "in",
       plot = melted_latelife_plot)
```


### With including training test splits 
```{r}
full_bayescom_chain <- import_parallel_models("full_model", "bayescomm_earlylife_train_5e4_")
full_corr_coef <- combine_bayescomm_corr_coef(full_bayescom_chain)
full_env_coef <- combine_bayescomm_env_coef(full_bayescom_chain)
```
```{r}
train_full_bayescom_chain <- import_parallel_models("full_model", "bayescomm_latelife_train_5e4_")
train_full_corr_coef <- combine_bayescomm_corr_coef(full_bayescom_chain)
train_full_env_coef <- combine_bayescomm_env_coef(full_bayescom_chain)

```

### The prediction 

```{r predicting_one}
predict(full_bayescom_chain[[1]])
```
