---
title: "stepwise_regression"
output: html_document
---

```{r setup, include=FALSE}
library(dplyr)
library(copathogenTools)

source(file.path("..", "scripts", "combining_env_data.R"), echo = F, chdir = T)
```

## Data 

Data is the complete data set with as many ENV variables as possible. 

```{r data_import}
newborn_complete <- newborn_w_timepoints %>% 
  select(-`Antibiotics, caregiver report [EUPATH_0000582]`,
         -`Myeloperoxidase (ng/mL) [EUPATH_0011277]`,
         -`Neopterin (nmol/L) [EUPATH_0011278]`, 
         -`Alpha-1-antitrypsin (mg/g) [EUPATH_0011276]`) %>%  
  filter(complete.cases(.))

infant_complete <- infant_w_timepoints %>% 
  select(-`Adjusted L:M excretion ratio z-score [EUPATH_0011439]`) %>% 
  filter(complete.cases(.))

newborn_PA <- newborn_complete %>% select(get_pathogens()) %>% copathogenTools::combine_like_pathogens()
newborn_env <- newborn_complete %>% select(-get_pathogens()) 

infant_PA <- infant_complete %>% select(get_pathogens()) %>% copathogenTools::combine_like_pathogens()
infant_env <- infant_complete %>% select(-get_pathogens()) 
```

```{r, env_data_prep}
# There is a warning for probability of 0, 1, I suspect this is because there's perfect seperattion of the outcome variable based on
# some of the predictor variables. 

fix_variables <- function(x){
  # print(length(x))
  if(is.character(x)){
    # if(length(unique(x)) > 2){
    #   print("multiple levels")
    # }
    if(all(sort(unique(x)) == sort(c("Yes", "No")))){
      # print("Length")
      # print(length(ifelse(x == "Yes", 1, 0)))
      # if(length(x) != length(ifelse(x == "Yes", 1, 0))){
        # print("Incorrect lengths")
      # }
      return(ifelse(x == "Yes", 1, 0))
    }#else if(is.numeric(x)){
    
    return(paste("__", x))
    
  }
  #   return(scale(x))
  # }else{
  #   return(x)
  # }
  return(x)
}

fix_env_data_infant <- function(env_data){
  final <- env_data %>% select(
    -participant, 
    -collection_date,
    -study, 
    -observation_id, 
    -bin_12, 
    -Household_Id, 
    -Participant_Id, 
    -`Age (days) [EUPATH_0000579]`, 
    -`Birth date [EFO_0004950]`, 
    -`Country [OBI_0001627]`, 
    -Household_Observation_Id, 
    -age_diff, 
    -age_diff_min
  ) %>% 
    ungroup() %>% 
    mutate(across(everything(), fix_variables))
  return(final)
  
}

# infant_env %>% fix_env_data_infant() %>% View()
```



## Model 
```{r functions}
select_abiotic_factors <- function(PA_data, env_data){
  
  n_steps <- 10 # default is 1000
  # Create a matrix with the results of the stepwise regression. 
  
  variable_selections <- matrix(0, nrow = ncol(PA_data), ncol = ncol(env_data))
  colnames(variable_selections) <- names(env_data)
  rownames(variable_selections) <- names(PA_data)
  
  models <- list()
  
  # For every pathogen in the multiple regression
  for(p in 1:ncol(PA_data)){
    # Get the minimum number of variables. 
    minimum_size <- glm(PA_data[,p] ~ 1, data = env_data, family = binomial(link = "probit"))
    
    # Get the Maximum number or variables. 
    maximum_size <- glm(PA_data[,p] ~ ., data = env_data, family = binomial(link = "probit"))
    
    # run the model selection 
    selected_model <- step(minimum_size, formula(maximum_size), direction = "forward", steps = n_steps)
    
    # add model to list 
    models <- c(models, list(selected_model))
    
    # Get the names of the selected coefficients 
    selected_variables <- names(selected_model$coefficients)
    variable_tally <- (1:length(selected_variables))[-grep('\\(', selected_variables)]
    
    selected_variables <- gsub("\`", "", selected_variables)
    
    fixed_categorical <- unique(gsub("__.*$", "", selected_variables[variable_tally]))
    # Update the variable selection matrix
    variable_selections[p, fixed_categorical] <- 1
  }
  return(variable_selections)
}

selected_variables <- select_abiotic_factors(
  infant_PA, 
  infant_env %>% fix_env_data_infant()
)

write.csv(selected_variables, file.path("..", "data", "variable_selection", "infant_selection.csv"), row.names = T)
```


```{r}
PA_data <-   infant_PA

env_data <- infant_env %>% fix_env_data_infant()

 n_steps <- 10 # default is 1000
  # Create a matrix with the results of the stepwise regression. 
  
  variable_selections <- matrix(0, nrow = ncol(PA_data), ncol = ncol(env_data))
  colnames(variable_selections) <- names(env_data)
  rownames(variable_selections) <- names(PA_data)
  
  models <- list()
  
  # For every pathogen in the multiple regression
  # for(p in 1:ncol(PA_data)){
    # Get the minimum number of variables. 
    minimum_size <- glm(PA_data[,2] ~ 1, data = env_data, family = binomial(link = "probit"))
    
    # Get the Maximum number or variables. 
    maximum_size <- glm(PA_data[,2] ~ ., data = env_data, family = binomial(link = "probit"))
    
    # run the model selection 
    selected_model <- step(minimum_size, formula(maximum_size), direction = "forward", steps = n_steps)
    
    # add model to list 
    models <- c(models, list(selected_model))
    
    # Get the names of the selected coefficients 
    selected_variables <- names(selected_model$coefficients)
    variable_tally <- (1:length(selected_variables))[-grep('\\(', selected_variables)]
    
    selected_variables <- gsub("\`", "", selected_variables)
    
    fixed_categorical <- unique(gsub("__.*$", "", selected_variables[variable_tally]))
    # Update the variable selection matrix
    variable_selections[2, fixed_categorical] <- 1
  # }
```




