---
title: "no_env_LVM"
output: html_document
---

# Implementing the LVM with NO ENV data. 

```{r setup}
library(jSDM)
```

```{r import_functions}
source(file.path("..", "scripts", "post_processing.R"), echo = F)
```

```{r testing_models}
test_model <- jSDM_binomial_probit_block(
  # Chains
  burnin=100, mcmc=600, thin=5,
  # Response variable 
  presence_site_sp = early_life_PA, 
  # Explanatory variables 
  site_suitability = ~ wet_season + stool_type + maternal_education + `South Africa` + Pakistan + Peru + Nepal + Bangladesh + 
    India + Tanzania + improved_water + urban + improved_toilet + breast_feeding_only, 
  site_data = early_life_env_cleaned,
  # Model specification 
  n_latent=0, site_effect="none",
  # Starting values
  alpha_start=0, beta_start=0,
  lambda_start=0, W_start=0,
  V_alpha=1, 
  # Priors
  shape=0.1, rate=0.1,
  mu_beta=0, V_beta=10,
  mu_lambda=0, V_lambda=10,
  # Various 
  seed=1234, verbose=1)


get_residual_cor(test_model)

test_enviro <- jSDM::get_enviro_cor(test_model)

corrplot::corrplot(test_enviro$sig.cor, title = "Shared response correlations",
                   outline = T,
         type = "lower", diag = FALSE, method = "color", mar = c(3,0.5,2,1), tl.srt = 45)

test_model_2lv <- jSDM_binomial_probit_block(
  # Chains
  burnin=100, mcmc=600, thin=5,
  # Response variable 
  presence_site_sp = early_life_PA, 
  # Explanatory variables 
  site_suitability = ~ wet_season + stool_type + maternal_education + `South Africa` + Pakistan + Peru + Nepal + Bangladesh + 
    India + Tanzania + improved_water + urban + improved_toilet + breast_feeding_only, 
  site_data = early_life_env_cleaned,
  # Model specification 
  n_latent=2, site_effect="none",
  # Starting values
  alpha_start=0, beta_start=0,
  lambda_start=0, W_start=0,
  V_alpha=1, 
  # Priors
  shape=0.1, rate=0.1,
  mu_beta=0, V_beta=10,
  mu_lambda=0, V_lambda=10,
  # Various 
  seed=1234, verbose=1)



test_res_2lv <- my_get_residual_corr(test_model_2lv)

test_enviro_2lv <- jSDM::get_enviro_cor(test_model_2lv)


corrplot::corrplot(test_res_2lv$sig.mean, title = "Shared response correlations",
                   outline = T,
         type = "lower", diag = FALSE, method = "color", mar = c(3,0.5,2,1), tl.srt = 45)


corrplot::corrplot(test_enviro_2lv$sig.cor, title = "Shared response correlations",
                   outline = T,
         type = "lower", diag = FALSE, method = "color", mar = c(3,0.5,2,1), tl.srt = 45)
```

## Fitting Models

### Community Model

```{r jsdm_models}

null_site <- data.frame(Int = rep(1, nrow(early_life_PA)))
community_only <- jSDM_binomial_probit_block(
  # Chains
  burnin=1000, mcmc=6000, thin=5,
  # Response variable 
  presence_site_sp = early_life_PA, 
  # Explanatory variables 
  site_suitability = ~Int-1, 
  site_data = null_site,
  # Model specification 
  n_latent=2, site_effect="none",
  # Starting values
  alpha_start=0, beta_start=0,
  lambda_start=0, W_start=0,
  V_alpha=1, 
  # Priors
  shape=0.1, rate=0.1,
  mu_beta=0, V_beta=10,
  mu_lambda=0, V_lambda=10,
  # Various 
  seed=1234, verbose=1)

community_only_more_iter <- jSDM_binomial_probit_block(
  # Chains
  burnin=10000, mcmc=60000, thin=50,
  # Response variable 
  presence_site_sp = early_life_PA, 
  # Explanatory variables 
  site_suitability = ~Int-1, 
  site_data = null_site,
  # Model specification 
  n_latent=2, site_effect="none",
  # Starting values
  alpha_start=0, beta_start=0,
  lambda_start=0, W_start=0,
  V_alpha=1, 
  # Priors
  shape=0.1, rate=0.1,
  mu_beta=0, V_beta=10,
  mu_lambda=0, V_lambda=10,
  # Various 
  seed=1234, verbose=1)

community_only_lv10 <- jSDM_binomial_probit_block(
  # Chains
  burnin=1000, mcmc=6000, thin=5,
  # Response variable 
  presence_site_sp = early_life_PA, 
  # Explanatory variables 
  site_suitability = ~Int-1, 
  site_data = null_site,
  # Model specification 
  n_latent=10, site_effect="none",
  # Starting values
  alpha_start=0, beta_start=0,
  lambda_start=0, W_start=0,
  V_alpha=1, 
  # Priors
  shape=0.1, rate=0.1,
  mu_beta=0, V_beta=10,
  mu_lambda=0, V_lambda=10,
  # Various 
  seed=1234, verbose=1)

community_only_lv10_more_iter <- jSDM_binomial_probit_block(
  # Chains
  burnin=10000, mcmc=60000, thin=50,
  # Response variable 
  presence_site_sp = early_life_PA, 
  # Explanatory variables 
  site_suitability = ~Int-1, 
  site_data = null_site,
  # Model specification 
  n_latent=10, site_effect="none",
  # Starting values
  alpha_start=0, beta_start=0,
  lambda_start=0, W_start=0,
  V_alpha=1, 
  # Priors
  shape=0.1, rate=0.1,
  mu_beta=0, V_beta=10,
  mu_lambda=0, V_lambda=10,
  # Various 
  seed=1234, verbose=1)
```

```{r}
# library(ggplot2)

ggplot(data = NULL)+
  geom_point(aes(x = seq(length(community_only$mcmc.sp$sp_30[,1])), y = community_only$mcmc.sp$sp_30[,1]))
# community_only$mcmc.sp$

ggplot(data = NULL)+
  geom_density(aes(x = community_only$mcmc.sp$sp_30[,1]))
# community_only$mcmc.sp$


ggplot(data = NULL)+
  geom_point(aes(x = seq(length(community_only$mcmc.sp$sp_30[,2])), y = community_only$mcmc.sp$sp_30[,2]))
# community_only$mcmc.sp$

ggplot(data = NULL)+
  geom_point(aes(x = seq(length(community_only$mcmc.sp$sp_30[,3])), y = community_only$mcmc.sp$sp_30[,3]))
# community_only$mcmc.sp$

coda::traceplot(community_only$mcmc.sp$sp_30)
```


### Enviornmental Variables

Fitting the ENV without any latent variables? 

- Confirm this is how this works. Make sure 0 is not actually 528. 
```{r env_only}
env_model <- jSDM_binomial_probit_block(
  # Chains
  burnin=10000, mcmc=60000, thin=50,
  # Response variable 
  presence_site_sp = early_life_PA, 
  # Explanatory variables 
  site_suitability = ~ wet_season + stool_type + maternal_education + `South Africa` + Pakistan + Peru + Nepal + Bangladesh + 
    India + Tanzania + improved_water + urban + improved_toilet + breast_feeding_only, 
  site_data = early_life_env_cleaned,
  # Model specification 
  n_latent=0, site_effect="none",
  # Starting values
  alpha_start=0, beta_start=0,
  lambda_start=0, W_start=0,
  V_alpha=1, 
  # Priors
  shape=0.1, rate=0.1,
  mu_beta=0, V_beta=10,
  mu_lambda=0, V_lambda=10,
  # Various 
  seed=1234, verbose=1)
# 
# env_model_10lv <- jSDM_binomial_probit_block(
#   # Chains
#   burnin=1000, mcmc=6000, thin=5,
#   # Response variable 
#   presence_site_sp = early_life_PA, 
#   # Explanatory variables 
#   site_suitability = ~ wet_season + stool_type + maternal_education + `South Africa` + Pakistan + Peru + Nepal + Bangladesh + 
#     India + Tanzania + improved_water + urban + improved_toilet + breast_feeding_only, 
#   site_data = early_life_env_cleaned,
#   # Model specification 
#   n_latent=, site_effect="none",
#   # Starting values
#   alpha_start=0, beta_start=0,
#   lambda_start=0, W_start=0,
#   V_alpha=1, 
#   # Priors
#   shape=0.1, rate=0.1,
#   mu_beta=0, V_beta=10,
#   mu_lambda=0, V_lambda=10,
#   # Various 
#   seed=1234, verbose=1)

```


## Full Model 

```{r fill_model}
full_model <- jSDM_binomial_probit_block(
  # Chains
  burnin=1000, mcmc=6000, thin=5,
  # Response variable 
  presence_site_sp = early_life_PA, 
  # Explanatory variables 
  site_suitability = ~ wet_season + stool_type + maternal_education + `South Africa` + Pakistan + Peru + Nepal + Bangladesh + 
    India + Tanzania + improved_water + urban + improved_toilet + breast_feeding_only, 
  site_data = early_life_env_cleaned,
  # Model specification 
  n_latent=2, site_effect="none",
  # Starting values
  alpha_start=0, beta_start=0,
  lambda_start=0, W_start=0,
  V_alpha=1, 
  # Priors
  shape=0.1, rate=0.1,
  mu_beta=0, V_beta=10,
  mu_lambda=0, V_lambda=10,
  # Various 
  seed=1234, verbose=1)



full_model_10lv <- jSDM_binomial_probit_block(
  # Chains
  burnin=1000, mcmc=6000, thin=5,
  # Response variable 
  presence_site_sp = early_life_PA, 
  # Explanatory variables 
  site_suitability = ~ wet_season + stool_type + maternal_education + `South Africa` + Pakistan + Peru + Nepal + Bangladesh + 
    India + Tanzania + improved_water + urban + improved_toilet + breast_feeding_only, 
  site_data = early_life_env_cleaned,
  # Model specification 
  n_latent=10, site_effect="none",
  # Starting values
  alpha_start=0, beta_start=0,
  lambda_start=0, W_start=0,
  V_alpha=1, 
  # Priors
  shape=0.1, rate=0.1,
  mu_beta=0, V_beta=10,
  mu_lambda=0, V_lambda=10,
  # Various 
  seed=1234, verbose=1)

```

## Results

### Community only

```{r community_chain_eval}
coda::geweke.diag(community_only$mcmc.sp$sp_1)$z[1]

names(community_only$mcmc.sp)

eval_community_w_geweke <- function(model){
  for(s in 1:length(names(model$mcmc.sp))){
    species <- paste0("sp_", s)
    geweke_eval <- coda::geweke.diag(community_only$mcmc.sp[[species]])
    for(b in 1:length(geweke_eval$z)){
      if(!is.nan(geweke_eval$z[b]) & abs(geweke_eval$z[b]) > 2){
        print(paste("No convergence for species", s))
        print(paste("For metric", b))
        print(geweke_eval$z[b])
      }
    }
  }
}


eval_community_w_geweke(community_only_lv10)


coda::geweke.plot(community_only$mcmc.sp$sp_3)
```
```{r}
coda::geweke.plot(community_only_lv10$mcmc.sp$sp_30)
```

```{r}
eval_community_w_geweke(community_only_more_iter$mcmc.sp$sp_30)

coda::geweke.plot(community_only_more_iter$mcmc.sp$sp_30)

coda::traceplot(community_only_more_iter$mcmc.sp$sp_30)
```



For a majority of the species, the parameters did not converge for most of the species, meaning I might need to run a couple chains. 

```{r, eval_functions}
community_only_corrs <- my_get_residual_corr(community_only)

sum(is.na(community_only_corrs$sig.mean))

biotic_interactions <- community_only_corrs$sig.mean

colnames(biotic_interactions) <- names(early_life_PA)
rownames(biotic_interactions) <- names(early_life_PA)

corrplot::corrplot(biotic_interactions, diag = F, type = "lower",
    outline = T,
    title = "Residual Correlation Matrix from LVM", 
    mar = c(1, 1, 3, 1), method = "color", tl.srt = 45, 
    tl.cex = 0.75)

# Next Step, what is the relationship between the percentile from the rewiring method and 
```
```{r corr_10LV}
community_only_corrs_10lv <- my_get_residual_corr(community_only_lv10)

biotic_interactions_10lv <- community_only_corrs_10lv$sig.mean

colnames(biotic_interactions_10lv) <- names(early_life_PA)
rownames(biotic_interactions_10lv) <- names(early_life_PA)

corrplot::corrplot(biotic_interactions_10lv, diag = F, type = "lower",
    outline = T,
    title = "Residual Correlation Matrix from LVM", 
    mar = c(1, 1, 3, 1), method = "color", tl.srt = 45, 
    tl.cex = 0.75)
```


```{r save_to_file}
dput(community_only, file = "../models/community_model_LVM/community_lv2.txt", control = "all")

dput(community_only_lv10, file = "../models/community_model_LVM/community_lv10.txt", control = "all")

dput(env_only, file = "../models/env_model/env_lv2.txt", control = "all")

dput(full_model, file = "../models/full_model/full_lv2.txt", control = "all")

dput(full_model_lv10, file = "../models/full_model/full_lv10.txt", control = "all")
```
## ENV Only, no community model


# Evaluating Parallel for LVM model

```{r eval_parallel_model }
import_env_only_models <- function(){
  all_models<- list()
  for(i in 1:4){
    print(file.path("..", "models", "env_only", paste(c("parallelmodel_", i, ".rda"), collapse = "")))
    all_models[[i]] <- readRDS(
      file = file.path("..", "models", "env_only", paste(c("parallelmodel_", i, ".rda"), collapse = "")))
    # View(all_models[[i]])
    # all_models[[i]] <- paste0(c("parallelmodel_", i, "rda"))
  }
  return(all_models)
}

all_fitted <- import_env_only_models()

```





