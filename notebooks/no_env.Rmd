---
title: "jsdm_no_env"
output: html_document
---

```{r setup}
library(dplyr)
library(ggplot2)
library(BayesComm)

source("../scripts/constants.R", echo = F)

```

```{r, functions}
archive_chain <- function(model, dir_name, trace_num){
  
  n_steps <- dim(model[['trace']][['z']])[1]
  n_sites <- dim(model[['trace']][['z']])[2]
  n_species <- dim(model[['trace']][['z']])[3]
  
  print(paste("Writing to File, Trace:", trace_num))
  write.csv(model[['trace']][['R']], file.path("../models", dir_name, paste0(c("R_", trace_num, '.csv'), collapse = "")), row.names = F)
  coefficient_df <- do.call(rbind, model[["trace"]][["B"]]) %>% data.frame() %>% 
    mutate(pathogens = rep(names(model[["trace"]][["B"]]), each = n_steps))
  write.csv(coefficient_df, file.path("../models", dir_name, paste0(c("B_", trace_num, ".csv"), collapse = "")), row.names = F)
  # outcome_matrix <- matrix(model[['trace']][['z']], n_steps*n_sites, n_species) %>% 
  #   data.frame() %>% mutate(trace_step = rep(seq(n_steps), n_sites), site = rep(seq(n_sites), each = n_steps))
  # names(outcome_matrix) <- c(names(model[['trace']][['R']]), "trace_step", "site")
  # write.csv(outcome_matrix, file.path("../models", dir_name, paste0(c("Z_", trace_num, ".csv"), collapse = "")), row.names = F)
  
}
```


```{r import}
early_life_data <- readr::read_csv("../data/early_life_env_data.csv") %>% select(-X1)
early_life_PA <- early_life_data %>% select(get_pathogens())
early_life_envs <- early_life_data %>% select(-get_pathogens(), -observation_id, -participant)
```
Started Approx 3:40
```{r init_model}
null_model <- BC(Y = as.matrix(early_life_PA), X = as.matrix(early_life_env_cleaned), model = "null", its = 600, verbose = 1, thin = 1, burn = 100)

```

```{r null_model_results, fig.height=10, fig.width = 10}
plot(null_model, "R")
```

```{r init_community_model}
dir.create(file.path("../models", "community_model"))
for(i in 1:4){
  temp_model <-  BC(
    Y = as.matrix(early_life_PA), X = NULL, model = "community", its = 6000, verbose = 1, thin = 50, burn = 1000)
  archive_chain(temp_model, "community_model", i)
  rm(temp_model)
}
# Two things: 

# Write to file so we can generate multiuple chains of the inference to check for convergingness. 

# Implement in pyro?
```
```{r community_model_results, fig.height=10, fig.width = 10}
plot(community_model, "R")
```

```{r import_from_file}
# Import the bayescomm_objects from file. 

import_trace <- function(dir_name){
  bayes_obj <- list()
  bayes_obj[['trace']] <- list()
  parent_dir <- "../models"
  r_files <- list.files(path = file.path("../models", dir_name), pattern = "R_[0-9].csv")
  r_all <- list()
  for(r in 1:length(r_files)){
    r_all[[r]] <- readr::read_csv(file.path("../models", dir_name, r_files[r]))%>% as.data.frame() %>% mutate(chain = r) %>% 
      mutate(t_step = seq(nrow(.))) %>% 
      tidyr::pivot_longer(-c("t_step", "chain"), names_to = "pathogen_pair", values_to = "coefficient")
  }
  
  bayes_obj[['trace']][['R']] <- do.call(rbind, r_all)
  b_files <- list.files(path = file.path("../models", dir_name), pattern = "B_[0-9].csv") 
  b_all <- list()

  for(b in 1:length(b_files)){
    b_all[[b]] <- readr::read_csv(file.path("../models", dir_name, b_files[b])) %>% as.data.frame() %>% 
      mutate(t_step = rep(seq(nrow(.)/length(unique(.$pathogens))), length(unique(.$pathogens))), chain = b)
  }
  
  bayes_obj[['trace']][['B']] <- do.call(rbind, b_all)
  
  return(bayes_obj)

  
}

imported_community_model <- import_trace("community_model")

```

```{r, evaluate_chains_coefficients}
imported_community_model[['trace']][['B']] %>% 
  ggplot(aes(x = t_step, y = intercept, color = as.factor(chain)))+
  geom_line()+
  facet_wrap(.~pathogens, scale = "free")

imported_community_model[['trace']][['B']] %>% 
  ggplot(aes(x = intercept, color = as.factor(chain)))+
  geom_density()+
  geom_rug()+
  facet_wrap(.~pathogens, scales = "free")

```
```{r evaluate_chains_correlations, fig.height = 20, fig.width = 20}
r_trace_chains <- imported_community_model[['trace']][['R']] %>% 
  ggplot(aes(x = t_step, y = abs(coefficient), color = as.factor(chain)))+
  geom_line()+
  facet_wrap(.~pathogen_pair, scale = "free")

ggsave(plot = r_trace_chains, filename = "../figures/community_model_R_traces.pdf", width = 40, device = "pdf", height = 40)

r_trace_density <- imported_community_model[['trace']][['R']] %>% 
  ggplot(aes(x = abs(coefficient), color = as.factor(chain)))+
  geom_density()+
  geom_rug()+
  facet_wrap(.~pathogen_pair, scales = "free")

ggsave(plot = r_trace_density, filename = "../figures/community_model_R_density.pdf", width = 40, device = "pdf", height = 40)

```
 
 The chains do not converge! We need to consider running for longer, or use the s-JSDM package. 

```{r community_model_large}
dir.create(file.path("../models", "community_model_10X"))
for(i in 1:4){
  temp_model <-  BC(
    Y = as.matrix(early_life_PA), X = NULL, model = "community", its = 60000, verbose = 1, thin = 500, burn = 10000)
  archive_chain(temp_model, "community_model_10X", i)
  rm(temp_model)
}
```


```{r large_community_model_import}
large_imported_community_model <- import_trace("community_model_10X")
```

```{r large_community_model_plot}
r_trace_chains <- large_imported_community_model[['trace']][['R']] %>% 
  ggplot(aes(x = t_step, y = coefficient, color = as.factor(chain)))+
  geom_line()+
  facet_wrap(.~pathogen_pair, scale = "free")

ggsave(plot = r_trace_chains, filename = "../figures/large_community_model_R_traces.pdf", width = 40, device = "pdf", height = 40)

r_trace_density <- large_imported_community_model[['trace']][['R']] %>% 
  ggplot(aes(x = coefficient, color = as.factor(chain)))+
  geom_density()+
  geom_rug()+
  facet_wrap(.~pathogen_pair, scales = "free")

ggsave(plot = r_trace_density, filename = "../figures/large_community_model_R_density.pdf", width = 40, device = "pdf", height = 40)
```

