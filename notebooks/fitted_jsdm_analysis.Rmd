---
title: "Post Fit Analysis"
output: html_document
---

```{r setup}
library(dplyr)
library(ggplot2)
source(file.path("..", "scripts", "data_import.R"), echo = F, chdir = T)
```


```{r functions}
# import_parallel_models <- function(directory, pattern){
#   all_models <- list()
#   for(i in 1:4){
#     file_name <- paste(c("parallelmodel_", pattern, ifelse(pattern != "", "_", ""), i, ".rda"), collapse = "")
#     all_models[[i]] <- readRDS(file = paste(file.path("..", "models", directory, file_name)))
#   }
#   return(all_models)
# }

combined_species_chains <- function(model){
  all_species_all_models <- list()
  for(m in 1:4){
    species_list <- list()
    for(s in 1:length(model[[m]]$mcmc.sp)){
      species_list[[s]] <- model[[m]]$mcmc.sp[[s]] %>% as.data.frame() %>% 
        mutate(species = s, model = m, iter = seq(nrow(.)))
    }
    species_single_model <- do.call(rbind, species_list)
    all_species_all_models[[m]] <- species_single_model
  }
  final <- do.call(rbind, all_species_all_models)
  return(final)
}

plot_jsdm_traces <- function(combined_df, path_names){
  for(s in 1:33){
    temp_plot <- combined_df %>% filter(species == s) %>%
      ggplot()+
      geom_point(aes(x = iter, y = values, color = as.factor(model))) + 
      facet_wrap(parameter~., scales = "free")+
      labs(title = path_names[s])
    print(temp_plot)
  }  
}

plot_jsdm_trace_densities <- function(combined_df, path_names){
  for(s in 1:33){
    temp_plot <- combined_df %>% filter(species == s) %>%
      ggplot(aes(x = values, color = as.factor(model)))+
      geom_density() + 
      geom_rug()+
      facet_wrap(parameter~., scale = "free") + 
      labs(title = path_names[s])
    print(temp_plot)
  }
}
```

# Community Only 

```{r community_only_traceplots}
# community_only <- import_parallel_models("community_only", "5e3burnin")
community_only <- import_parallel_models("community_only", "jsdm_15e3_")
community_only_species_df <- combined_species_chains(community_only) %>% 
  tidyr::pivot_longer(-c("model", "species", 'iter'), names_to = "parameter", values_to = "values")
```

```{r community_trace_plot}
plot_jsdm_traces(community_only_species_df, names(early_life_PA))
```

```{r community_only_density}
# There's Some isssue with the posterier distribution, perhaps there's a way to constrain the p
plot_jsdm_trace_densities(community_only_species_df, names(early_life_PA))
```

```{r community_only_import}
# 
# community_only <- import_parallel_models("community_only")
# community_only_species_df <- combined_species_chains(community_only) %>% 
#   tidyr::pivot_longer(cols = c("beta_Int", "lambda_1", "lambda_2"), names_to = "parameter", values_to = "values")

```


They're not good, we'll try a more iterations before expanding the number of latent variables. Then we might try fudgin the priors. 

# ENV only

```{r env_only_model}
# env_model <- import_parallel_models("env_only", "5e3burnin")
env_model <- import_parallel_models("env_only", "jsdm_15e3_")

env_model_species_df <- combined_species_chains(env_model) %>% 
  tidyr::pivot_longer(-c("model", "species", 'iter'), names_to = "parameter", values_to = "values")
```
```{r env_only_trace, fig.height = 10, fig.width = 10}
plot_jsdm_traces(env_model_species_df, names(early_life_PA))
```


```{r env_only_density}
plot_jsdm_trace_densities(env_model_species_df, names(early_life_PA))
```

# Full Model

```{r full_model_import}
# full_model <- import_parallel_models("full_model", "largeiter")
full_model <- import_parallel_models("full_model", "jsdm_15e3_")

full_model_species_df <- combined_species_chains(full_model) %>% 
  tidyr::pivot_longer(-c("model", "species", 'iter'), names_to = "parameter", values_to = "values")
```

```{r}
import_parallel_large_traces <- function(directory){
  all_models <- list()
  original_length <- 50000/5
  species_dataframe_list <- list()
  latent_dataframe_list <- list()

  for(m in 1:4){
    file_name <- paste(c("parallelmodel_largeiter_", m, ".rda"), collapse = "")
    all_models[[m]] <- readRDS(file = paste(file.path("..", "models", directory, file_name)))
    species_list <- list()
    for(s in 1:length(all_models[[m]]$mcmc.sp)){
      species_list[[paste0("sp_", s)]] <- all_models[[m]]$mcmc.sp[[s]][seq(0, original_length, 10)[-1],] %>% 
        as.data.frame() %>% 
        mutate(species = s, model = m, iter = seq(nrow(.)))
    }
    # all_models[[m]]$mcmc.sp <- species_list 
    species_dataframe_list[[m]] <- do.call(rbind, species_list)
    latent_list <- list()
    for(l in 1:length(all_models[[m]]$mcmc.latent)){
      latent_list[[paste0("lv_", l)]] <- all_models[[m]]$mcmc.latent[[l]][seq(0, original_length, 10)[-1],]  %>% 
        as.data.frame() %>% 
        mutate(lv = l, model = m, iter = seq(nrow(.)))
    }
    latent_dataframe_list[[m]] <- do.call(rbind, latent_list)
    all_models[[m]] <- NULL
    # all_models[[m]]$mcmc.latent <- latent_list
    
    # all_models[[m]]$mcmc.Deviance <- all_models[[m]]$mcmc.Deviance[seq(0, original_length, 10)[-1],]
    
  }
  final <- list()
  final[['latent']] <- do.call(rbind, latent_dataframe_list)
  final[['species']] <- do.call(rbind, species_dataframe_list)
  return(final)
}

full_model <- import_parallel_large_traces("full_model", "")
# full_model[[1]]$mcmc.Deviance
full_model_species_df <- full_model[['species']] %>% 
  tidyr::pivot_longer(-c("model", "species", 'iter'), names_to = "parameter", values_to = "values")
```



```{r chains_fullmodel}
plot_jsdm_traces(full_model_species_df, names(early_life_PA))
```

```{r density_fullmodel, fig.height = 9, fig.width = 12}
plot_trace_densities(full_model_species_df, names(early_life_PA))
```


```{r}
jSDM::get_enviro_cor(full_model[[1]])

# jSDM::get_enviro_cor()
```

