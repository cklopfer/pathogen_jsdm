---
title: "signed_variable_response"
output: html_document
---

```{r setup, include=FALSE}
library(dplyr)
library(ggplot2)
library(ggtext)
library(gridExtra)

source(file.path("..", "scripts", "transferred_functions.R"), echo = F)
source(file.path("..", "scripts", "constants.R"), echo = F)
source(file.path("..", "scripts", "variable_types.R"), echo = F)
source(file.path("..", "scripts", "visualisations.R"), echo = F)

```

```{r, functions}
label_pathogen_type <- function(path1){
  pathogen_labels <- list()

  pathogen_labels[["viruses"]] <- c(
    "adenovirus f"                  = "Adenovirus 40/41",
    "norovirus gi"                  = "Norovirus GI",
    "norovirus gii"                 = "Norovirus GII",
    "astrovirus"                    = "Astrovirus",
    "sapovirus"                     = "Sapovirus",
    "rotavirus"                     = "Rotavirus"#,
  )
  pathogen_labels[["bacteria"]] <- c(
    "aeromonas"                     = "Aeromonas",
    "salmonella"                    = "Salmonella",
    "h.pylori"                      = "H. pylori",
    "c.jejuni/coli"                 = "C. jejuni/coli",
    "campy pan"                     = "Campylobacter spp.",
    "b.fragilis"                    = "B. fragilis",
    "c.difficile"                   = "C. difficile",
    "m.tb"                          = "M. tuberculosis",
    "v.cholerae"                    = "V. cholerae",
    "shigella & eiec"               = "Shigella spp.",
    "eaec"                          = "EAEC",
    "atypical epec"                 = "aEPEC",
    "typical epec"                  = "tEPEC",
    "stec"                          = "STEC",
    "lt_etec"                       = "ETEC lt",
    "st_etec"                       = "ETEC st",
    "etec"                          = "ETEC", 
    "epec"                          = "EPEC"
  )
  
  pathogen_labels[["other"]] <- c(
    # "aeromonas"                     = "Aeromonas",
    "ancyclostoma"                  = "Ancyclostoma",
    "ascaris lumbricoides"          = "A. lumbricoides", 
    "trichuris trichiura"           = "Trichuris",
    "e.bieneusi"                    = "E. bieneusi",
    "e.intestinalis"                = "E. intestinalis",
    "cryptosporidium"               = "Cryptosporidium spp.",
    "necator"                       = "Necator",
    "strongyloides"                 = "Strongyloides",
    "cyclospora"                    = "Cyclospora",
    "isospora"                      = "Isospora",
    "e.histolytica"                 = "E. histolytica"#,
  )
  
  path1_temp <- NA
  pathogen_types <- vector(mode = "character", length = length(path1))
  print(names(pathogen_labels))
  for(t in names(pathogen_labels)){
    # if(path1 %in% names(pathogen_labels[[t]])){
    # print(t)
    print(sum(which(pathogen_labels[[t]] %in% path1)))
      pathogen_types[which(path1 %in% names(pathogen_labels[[t]]) )] <- t
    # }
  }
  
  return(pathogen_types)
}

rename_pathogens_from_numbers <- function(path_num, path_names){
  paths <- get_pathogens()
  names(path_names) <- as.character(seq(length(path_names)))
  print(path_names)
  
  return(plyr::revalue(as.character(path_num), path_names))
}

rename_env_variables <- function(){
  
  name_labels <- c(
      "Age 1st solid food given " = "Age 1st solid food given (days) ",
      "Cumulative ALRI episode count " = "Cumulative ALRI episode count ",
      "Cumulative days within diarrheal episodes " = "Cumulative days within diarrheal episodes ",
      "Loose stools in day, caregiver report " = "Loose stools in day",
      "Family income " = "Family income (local currency) ",
      "Chair " = "Chair ",
      "Dairy reported at last visit " = "Dairy reported at last visit ",
      "Fever, caregiver report " = "Fever, caregiver report ",
      "Infant formula " = "Infant formula ",
      "Table " = "Table ",
      # "Use of antibiotics, caregiver report " , 
      "Prelacteal feeding " = "Prelacteal feeding ",
      "improved_waste_disposal)" = "Improved Waste Disposal", 
      "wet_season)" = "Wet Season",
      "Appetite, caregiver report " = "Appetite",
      "breastfed_status_combined" = "Breastfeeding Status",
      "country)" = "Study Site",
      "Sex " = "Sex",
      "specific_partition)" = "Age (Timepoint)",
      "stool_type)" = "Diarrhea",
      "Age last exclusively breastfed " = "Age last exclusively breastfed (days) ",
      "Daily sugars " = "Daily sugars (g) ",
      "Fish or shellfish count "= "Fish or shellfish count ",
      "Meat count " = "Meat count ",
      "Myeloperoxidase " = "Myeloperoxidase (ng/mL) ",
      "ORT administered, caregiver report " = "ORT administered, caregiver report ", 
      "Refrigerator " = "Refrigerator ", 
      "Stool blood present " = "Stool blood present "
  )
  return(name_labels)
}

graph_partitions <- function(){
  
  partitions <- list(
    "continuous" = c(
      "Age 1st solid food given (days) ",
      "Cumulative ALRI episode count ",
      "Cumulative days within diarrheal episodes ",
      "Loose stools in day",
      "Family income (local currency) ",
      "Age last exclusively breastfed (days) ",
      "Daily sugars (g) ",
      "Fish or shellfish count ",
      "Meat count ",
      "Myeloperoxidase (ng/mL) "
      
    ), 
    'binary' = c(
      "Chair ",
      "Dairy reported at last visit ",
      "Fever, caregiver report ",
      "Infant formula ",
      "Table ",
      "Prelacteal feeding ",
      "Improved Waste Disposal", 
      "Wet Season",
      "Appetite",
      "Diarrhea",
      "ORT administered, caregiver report ", 
      "Refrigerator ", 
      "Stool blood present ",
      "Sex"
    
    ), 
    'categorical' = c(
      "Breastfeeding Status",
      "Study Site",
      "Age (Timepoint)"
      
    )
  )
  return(partitions)
}

abiotic_variable_types <- function(){
  
  variable_types <- list(
    "Location" = c(
      "Study Site"
      
    ), 
    
    "Milestones" = c(
      "Age 1st solid food given (days) ",
      "Age last exclusively breastfed (days) "
      
    ), 
    
    "Nutrition" = c(
      "Dairy reported at last visit ",
      "Infant formula ",
      "Prelacteal feeding ",
      "Breastfeeding Status",
      "Daily sugars (g) ",
      "Fish or shellfish count ",
      "Meat count "
      
    ), 
    
    "Socio-Economic" = c(
      "Family income (local currency) ",
      "Chair ",
      "Table ",
      "Refrigerator " 
      
    ), 
    
    "Time" = c(
      "Wet Season",
      "Age (Timepoint)"
      
    ), 
    
    "Clinical Presentation" = c(
      "Cumulative ALRI episode count ",
      "Cumulative days within diarrheal episodes ",
      "Loose stools in day",
      "Fever, caregiver report ",
      "Appetite",
      "Diarrhea",
      "Myeloperoxidase (ng/mL) ",
      "ORT administered, caregiver report ",
      "Stool blood present "
      
    ), 
    
    "Sanitation" = c(
       "Improved Waste Disposal" 
      
    ), 
    "Demographics" = c(
      "Sex"
    )
    
  )
  return(variable_types)
}

assign_variable_type <- function(d_f){
  var_types <- vector(mode = "character", length = nrow(d_f))
  type_labels <- abiotic_variable_types()
  for(n in names(type_labels)){
    var_types[which(d_f$cleaned_base_var %in% type_labels[[n]])] <- n
  }
  final <- d_f %>% mutate(var_type = var_types)
  return(final)
}
```

```{r, data_import}
newborn_split_dataset <- readr::read_csv(file.path("..", "data", paste("newborn", "variable_selection_training_crossval.csv", sep = "_"))) %>% 
  combine_like_pathogens()

infant_split_dataset <- readr::read_csv(file.path("..", "data", paste("infant", "variable_selection_training_crossval.csv", sep = "_"))) %>% 
  combine_like_pathogens()


n_pathogens <- newborn_split_dataset %>% select(get_pathogens()) %>% ncol() 

pathogen_names <- infant_split_dataset %>% select(get_pathogens()) %>% names()

all_data <- newborn_split_dataset %>% group_by(k_partition) %>% summarise(count = n())

infant_selection <- readr::read_csv(file.path("..", "data", "variable_selection", "final", "infant_selection_05Aug21.csv"))

newborn_selection <- readr::read_csv(file.path("..", "data", "variable_selection", "final", "newborn_selection_05Aug21.csv"))
```

```{r infant_melt}
infant_tidy <- infant_selection %>% 
  rename(pathogen = X1) %>% 
  tidyr::pivot_longer(-c("pathogen", "did_converge", "k_partition", "pathogen_num"), names_to = "abiotic_factor", values_to = "needed") %>% 
  mutate(abiotic_factor = gsub("\\[.*\\]", "", abiotic_factor)) %>% 
  mutate(pathogen = rename_pathogens_from_numbers(pathogen_num, pathogen_names))

newborn_tidy <- newborn_selection %>% 
  rename(pathogen = X1) %>% 
  # mutate(pathogen = pathogen_num) %>% 
  tidyr::pivot_longer(-c("pathogen", "did_converge", "k_partition", "pathogen_num"), names_to = "abiotic_factor", values_to = "needed") %>% 
  mutate(abiotic_factor = gsub("\\[.*\\]", "", abiotic_factor), 
         needed = case_when(is.na(needed) ~ 0, TRUE ~ needed))  %>% 
  mutate(pathogen = rename_pathogens_from_numbers(pathogen_num, pathogen_names))

# NOTE: There's a cross validation that's missing a variable. These missing variables are for variables that do not have any unique values in their specific partition. These are treasted as negative values because they're uninformative. 
```

```{r, message=F, warning=F}
build_formula_string <- function(data_subset, var_list){
  #  Get all the variables that are needed in the model. 
  variables_needed <- data_subset$abiotic_factor
  string_list <- vector(mode = "character", length = nrow(data_subset))
  for(r in 1:nrow(data_subset)){
    # print(variables_needed[r])
    # print(var_list[grep(variables_needed[r], var_list, fixed = T)])
    string_list[r] <- var_list[grep(variables_needed[r], var_list, fixed = T)][[1]]
    # string_list[r] <- ifelse(!grepl("factor", string_list[r]), gsub("\\`", "", string_list[r]), string_list[r])
  }
  final <- paste(string_list, collapse = " + ")
  return(final)
}

find_signed_response <- function(var_selection, og_dataset, var_list){
  #' Find the signed response from the glm model. 
  #' 
  #' Var_selection: the dataframe in tidy format. 
  
  PA_data <- og_dataset %>% select(any_of(get_pathogens()))
  ENV_data <- og_dataset %>% select(-any_of(get_pathogens()))
  # a list to hold all partitions
  #start a counter
  counter <- 1
  formula_list <- list()
  dataframe_list <- list()
  # For every pathogen, generate a model from the glm with a probit link. 
  # For each k-fold partition. 
  for(p in unique(var_selection$pathogen)){
    for(k in unique(var_selection$k_partition)){
      
      var_subset <- var_selection %>% filter(pathogen == p, k_partition == k, needed == 1, did_converge == 1)
      if(nrow(var_subset) > 0){
        pathogen_formula <- glm(as.formula(
          paste(paste0("`", p, "`"), build_formula_string(var_subset, var_list), sep = "~")), family = binomial(link = "probit"), 
          data = og_dataset %>% filter(k_partition == k))

        var_w_values <-  cbind(signed_values = pathogen_formula$coefficients[-1], 
                                              labels = names(pathogen_formula$coefficients)[-1]) %>% 
          as.data.frame() %>% 
          mutate(pathogen = p, k_partition = k)
        
        formula_list[[counter]] <- pathogen_formula
        dataframe_list[[counter]] <- var_w_values
        counter <- counter + 1
        
      }
      # get formula and append as string.
    }
  }
  final_df <- do.call(rbind, dataframe_list)

  return(list(final_df, formula_list))
  
}

fix_regression_output <- function(regression_output){
  cleaned_vars <- regression_output %>% mutate( base_var = gsub("(factor\\()|\\n\\s+|\\)$", "", labels, perl = T)) %>%
    mutate(base_var = gsub("([1-9]+$)|(\\_\\_\\s.+$)","", base_var, perl = T)) %>% 
    mutate(base_var = gsub("`","", base_var))
  
  cleaned_coef <- gsub("^.+\\)", "", regression_output$labels, perl = T)
  cleaned_coef <- gsub("\\`", "", cleaned_coef)
  cleaned_coef[grep("EUPATH", cleaned_coef)] <- NA
  cleaned_coef[cleaned_coef == regression_output$labels] <- NA
  
  cleaned_sample_formulas <- cleaned_vars %>% ungroup() %>% mutate(chosen_level = cleaned_coef)
  return(cleaned_sample_formulas)
}

newborn_signed_response <- find_signed_response(newborn_tidy, newborn_split_dataset, variable_types)
cleaned_newborn_signed_response <- fix_regression_output(newborn_signed_response[[1]])

infant_signed_response <- find_signed_response(infant_tidy, infant_split_dataset, variable_types)
cleaned_infant_signed_response <- fix_regression_output(infant_signed_response[[1]])

```

```{r newborn_response}
# color_generattor <- colorRampPalette(RColorBrewer::brewer.pal(9, "Set1"))
generate_signed_response_newborn <- function(d_f, var_types, model_type){
  datatype_partitions <- graph_partitions()
  figure_directory <- file.path("..", "figures", "variable_selection", "newborn")
  image_resolution <- 300
  
  full_df <- d_f %>% group_by(pathogen, base_var) %>% mutate(n_parts = n_distinct(k_partition)) %>% 
    filter(n_parts == 10) %>%
    ungroup() %>% group_by(base_var) %>% mutate(n_paths = n_distinct(pathogen)) %>%
    filter(n_paths > 1) %>%
    mutate(base_var = gsub("\\[.+", "", base_var)) %>% 
    mutate(base_var = gsub("(\\(|\\)).+", "", base_var, perl = T)) %>%
    ungroup() %>% 
    mutate(cleaned_base_var = plyr::revalue(base_var, replace = rename_env_variables()), 
           chosen_level = gsub("\\_\\_ ", "", chosen_level), 
           pathogen = plyr::revalue(pathogen, replace = get_pathogen_labels(formatted = T))) %>% 
    mutate(chosen_level = case_when(
      (cleaned_base_var == "Diarrhea") & (chosen_level == "Diarrhea")  ~ "1",
      (cleaned_base_var == "Appetite") & (chosen_level == "Normal appetite")  ~ "1",
      (cleaned_base_var == "Sex") & (chosen_level == "Male")  ~ "1",
      TRUE ~ as.character(chosen_level)
      )) %>% 
    mutate(chosen_level = case_when(is.na(chosen_level) ~ "Not Applicable", 
           TRUE ~ chosen_level)) %>% 
    group_by(pathogen, cleaned_base_var, chosen_level) %>% 
    mutate(max_response = max(as.numeric(signed_values)), 
           min_response = min(as.numeric(signed_values))) %>% ungroup() %>% 
    assign_variable_type()
    
  print(unique(full_df$var_type))

  ## BINARY NEWBORN
  temp_plot <- full_df %>% filter(cleaned_base_var %in% datatype_partitions[["binary"]]) %>% 
    mutate(var_type = factor(var_type, 
                             levels = c("Location","Clinical Presentation","Nutrition","Demographics",
                                        "Socio-Economic", "Time","Milestones","Sanitation"), ordered = T)) %>% 
        ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = var_type))+
    geom_boxplot()+
        # geom_errorbar(aes(ymin = min_response, ymax = max_response, color = var_type), 
        #               position = "dodge", size = 1.1 )+
        geom_hline(yintercept = 0, color = "gray40", size = 0.75, linetype = "dashed")+
        coord_flip()+
        scale_color_discrete()+
        theme(
          panel.background = element_blank(), 
          panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
          panel.grid.major.x = element_line(color = "gray90"), 
          panel.grid.major.y = element_line(color = "gray90"), 
          strip.text = element_text(angle = 0, size = rel(0.75)), 
          axis.text.x = element_text(size = rel(0.9)), 
          axis.text.y = element_markdown()
        )+
        facet_wrap(var_type~cleaned_base_var, ncol = 3,
                   scale = "free_x"
                   )+
        labs(title = "Binary Response Variables in Newborn Model", 
             x = "", 
             color = "Variable Category",
             y = "Coefficient Value")
  png(file.path(figure_directory,"newborn_binary.png"), height = 11, width = 10, units = "in", res = image_resolution)
  print(temp_plot)
  dev.off()
  
  print("Completed binary")
  
  ## Continuous Newborn Model 
    temp_plot <- full_df %>% filter(cleaned_base_var %in% datatype_partitions[["continuous"]]) %>% 
       mutate(var_type = factor(var_type, 
                             levels = c("Location","Clinical Presentation","Nutrition","Demographics",
                                        "Socio-Economic", "Time","Milestones","Sanitation"), ordered = T)) %>% 
      ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = var_type))+
      geom_boxplot()+
        # geom_errorbar(aes(ymin = min_response, ymax = max_response), 
        #               position = "dodge", size = 1.1, color = "darkslateblue")+
        geom_hline(yintercept = 0, color = "gray40", size = 0.75, linetype = "dashed")+
        coord_flip()+
        scale_color_discrete()+
        theme(
          panel.background = element_blank(),
          panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
          panel.grid.major.x = element_line(color = "gray90"),
          panel.grid.major.y = element_line(color = "gray90"),
          strip.text = element_text(angle = 0),
          axis.text.x = element_text(),
          axis.text.y = element_markdown()
        )+
        facet_wrap(var_type~cleaned_base_var, ncol = 3,
                   scale = "free_x"
                   )+
        labs(title = "Continuous Reponse Variables in Newborn Model", 
             x = "", y = "Coefficient Value", color = "")
  png(file.path(figure_directory,"newborn_continuous.png"), height = 6, width = 11, units = "in", res = image_resolution)
  print(temp_plot)
  dev.off() 
  
  
  # Breastfeeding Status 
    temp_plot <- full_df %>% filter(cleaned_base_var  == "Breastfeeding Status") %>% 
        ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = chosen_level))+
        # geom_violin(size = 2)+
        geom_boxplot(position = position_dodge(1))+
        geom_hline(yintercept = 0, color = "gray40", size = 1.2, linetype = "dashed")+
        coord_flip()+
        scale_color_manual(values = c('black', '#9ebcda', '#8856a7'), 
                           labels = c("Exclusively Breastfed (reference)", "Not Breastfed", "Partially Breastfed"), 
                           limits = c("exclusive", "not", "partial"),
                       drop = FALSE)+
        theme(
          panel.background = element_blank(), 
          panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
          panel.grid.major.x = element_line(color = "gray90"), 
          panel.grid.major.y = element_line(color = "gray90"), 
          strip.text = element_text(angle = 0), 
          axis.text.x = element_text(),
          plot.title = element_text(size = rel(2)),
          legend.text = element_text(size = rel(1.8)),
          axis.text.y = element_markdown(size = rel(1.8))
        )+
        labs(title = "Response to Breastfeeding Status", 
             x = "",
             color = "",
             y = "Coefficient Value")
  
  png(file.path(figure_directory,"breastfeeding_newborn.png"), height = 4, width = 10, units = "in", res = image_resolution)
  print(temp_plot)
  dev.off()
    
  # Sutdy Site 
  temp_plot <- full_df %>% filter(cleaned_base_var  == "Study Site") %>% 
          # filter(cleaned_base_var == p) %>% 
          ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = chosen_level))+
          # geom_errorbar(aes(ymin = min_response, ymax = max_response), position = "dodge", size = 0.5)+
          # geom_violin(size = 2)+
          geom_boxplot(position = position_dodge(1))+
          coord_flip()+
          # scale_color_manual(values = color_generattor(15))+
          facet_grid(pathogen ~. , scales = "free_y")+
          theme(
            panel.background = element_blank(), 
            panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
            panel.grid.major.x = element_line(color = "gray90"), 
            panel.grid.major.y = element_line(color = "gray90"), 
            strip.text = element_blank(),
            strip.background = element_blank(),
            plot.title = element_text(size = rel(2)),
            legend.text = element_text(size = rel(1.8)),
            axis.text.y = element_markdown(size = rel(1.8)),
            # axis.text.x = element_text(angle = 30, vjust = 0)
          )+
          scale_color_manual(
            limits = c("Bangladesh (reference)", "Brazil", "India", "Nepal", "Pakistan", "Peru", "South Africa", "Tanzania"),
            values = c("#666666","#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d"),
            drop = F)+
          labs(title = "Response to Study Site in Newborn Model", 
               x = "", 
               y = "Coefficient Value", 
               color = "")
  png(file.path(figure_directory,"study_site_newborn.png"), height = 12, width = 10, units = "in", res = image_resolution)
  print(temp_plot)
  dev.off()
  
  
  # NEWBORN TIMEPOINT
  temp_plot <- full_df %>% filter(cleaned_base_var  == "Age (Timepoint)") %>% 
          # filter(cleaned_base_var == p) %>% 
          ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = chosen_level))+
          # geom_violin(size = 2)+
          geom_boxplot(position = position_dodge(1))+
          geom_hline(yintercept = 0, color = "gray40", size = 1.2, linetype = "dashed")+
          coord_flip()+
          scale_color_manual(
            limits = c("1 month (reference)", "3 month", "6 month"), 
            values = c("#666666", "#7fc97f", "#8da0cb")
          )+
          facet_grid(pathogen ~. , scales = "free_y")+
          theme(
            panel.background = element_blank(), 
            panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
            panel.grid.major.x = element_line(color = "gray90"), 
            panel.grid.major.y = element_line(color = "gray90"), 
            strip.text = element_blank(),
            strip.background = element_blank(),
            axis.text.x = element_text(), 
            plot.title = element_text(size = rel(2)),
            axis.text.y = element_markdown(size = rel(1.8)),
            legend.text = element_text(size = rel(1.8))
          )+
          labs(title = "Response to Discrete Timepoint in Newborn Model", 
               y = "Coefficient Value", x = "", 
               color = "")
  png(file.path(figure_directory,"timepoint_newborn.png"), height = 15, width = 10, units = "in", res = image_resolution)
  print(temp_plot)
  dev.off()
  
}


generate_signed_response_newborn(cleaned_newborn_signed_response, graph_partitions())

```

```{r infant_response}
generate_signed_response_infant <- function(d_f, var_types, model_type){
  datatype_partitions <- graph_partitions()
  figure_directory <- file.path("..", "figures", "variable_selection", "infant")
  image_resolution <- 300
  
  full_df <- d_f %>% group_by(pathogen, base_var) %>% mutate(n_parts = n_distinct(k_partition)) %>% 
    filter(n_parts == 10) %>%
    ungroup() %>% group_by(base_var) %>% mutate(n_paths = n_distinct(pathogen)) %>%
    filter(n_paths > 1) %>%
    mutate(base_var = gsub("\\[.+", "", base_var)) %>% 
    mutate(base_var = gsub("(\\(|\\)).+", "", base_var, perl = T)) %>%
    ungroup() %>% 
    mutate(cleaned_base_var = plyr::revalue(base_var, replace = rename_env_variables()), 
           chosen_level = gsub("\\_\\_ ", "", chosen_level), 
           pathogen = plyr::revalue(pathogen, replace = get_pathogen_labels(formatted = T))) %>% 
    mutate(chosen_level = case_when(
      (cleaned_base_var == "Diarrhea") & (chosen_level == "Diarrhea")  ~ "1",
      (cleaned_base_var == "Appetite") & (chosen_level == "Normal appetite")  ~ "1",
      (cleaned_base_var == "Sex") & (chosen_level == "Male")  ~ "1",
      TRUE ~ as.character(chosen_level)
      )) %>% 
    mutate(chosen_level = case_when(is.na(chosen_level) ~ "Not Applicable", 
           TRUE ~ chosen_level)) %>% 
    group_by(pathogen, cleaned_base_var, chosen_level) %>% 
    mutate(max_response = max(as.numeric(signed_values)), 
           min_response = min(as.numeric(signed_values))) %>% ungroup() %>% 
    assign_variable_type()

  print(unique(full_df$var_type))


  ## BINARY 
  temp_plot <- full_df %>% filter(cleaned_base_var %in% datatype_partitions[["binary"]]) %>% 
    mutate(var_type = factor(var_type, 
                             levels = c("Location", "Clinical Presentation", "Milestones", 
                                        "Nutrition", "Socio-Economic", "Time", "Demographics" ))) %>% 
        ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = var_type))+
        geom_boxplot()+
    # geom_errorbar(aes(ymin = min_response, ymax = max_response), position = "dodge", size = 1.1)+
        geom_hline(yintercept = 0, color = "gray40", size = 0.75, linetype = "dashed")+
        coord_flip()+
        # scale_color_discrete(guide = "none")+
        theme(
          panel.background = element_blank(), 
          panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
          panel.grid.major.x = element_line(color = "gray90"), 
          panel.grid.major.y = element_line(color = "gray90"), 
          strip.text = element_text(angle = 0, size = rel(0.75)), 
          axis.text.x = element_text(),
          axis.text.y = element_markdown()
        )+
        facet_wrap(var_type~cleaned_base_var, ncol = 3,
                   scale = "free_x"
                   )+
        labs(title = "Binary Response Variables in Infant Model", 
             y = "Coefficient Value", 
             color = "Variable Category",
             x = "")
  png(file.path(figure_directory,"infant_binary.png"), height = 10, units = "in", width = 10, res = image_resolution)
  print(temp_plot)
  dev.off()
  
  print("Completed binary")
  # print(datatype_partitions[["contin"]])
  ## Continuous
    temp_plot <- full_df %>% filter(cleaned_base_var %in% datatype_partitions[["continuous"]]) %>% 
       mutate(var_type = factor(var_type, 
                             levels = c("Location", "Clinical Presentation", "Milestones", 
                                        "Nutrition", "Socio-Economic", "Time", "Demographics" ))) %>% 
      ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = var_type))+
        # geom_errorbar(aes(ymin = min_response, ymax = max_response), position = "dodge", size = 1.1)+
      geom_boxplot()+
        geom_hline(yintercept = 0, color = "gray40", size = 0.75, linetype = "dashed")+
        coord_flip()+
        # scale_color_discrete(guide = "none")+
        theme(
          panel.background = element_blank(),
          panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
          panel.grid.major.x = element_line(color = "gray90"),
          panel.grid.major.y = element_line(color = "gray90"),
          strip.text = element_text(angle = 0),
          axis.text.x = element_markdown(),
          axis.text.y = element_markdown()
        )+
        facet_wrap(var_type~cleaned_base_var, ncol = 3,
                   scale = "free_x"
                   )+
        labs(title = "Continuous Reponse Variables in Infant Model", 
             x = "", y = "Coefficient Value", color = "")
  png(file.path(figure_directory,"infant_continuous.png"), height = 6, units = "in", width = 10, res = image_resolution)
  print(temp_plot)
  dev.off()  
  # Breastfeeding Status 
    temp_plot <- full_df %>% filter(cleaned_base_var  == "Breastfeeding Status") %>% 
        ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = chosen_level))+
        # geom_violin(size = 2)+
        geom_boxplot(position = position_dodge(1))+
        geom_hline(yintercept = 0, color = "gray40", size = 1.2, linetype = "dashed")+
        coord_flip()+
        scale_color_manual(values = c('black', '#9ebcda', '#8856a7'), 
                           labels = c("Exclusively Breastfed (reference)", "Not Breastfed", "Partially Breastfed"), 
                           limits = c("exclusive", "not", "partial"),
                       drop = FALSE)+
        theme(
          panel.background = element_blank(), 
          panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
          panel.grid.major.x = element_line(color = "gray90"), 
          panel.grid.major.y = element_line(color = "gray90"), 
          strip.text = element_text(angle = 0), 
          axis.text.x = element_text(), 
          plot.title = element_text(size = rel(2)),
          legend.text = element_text(size = rel(1.8)),
          axis.text.y = element_markdown(size = rel(1.8)),
        )+
        labs(title = "Response to Breastfeeding Status", 
             y = "Coefficient Value",
             color = "",
             x = "")
  
  png(file.path(figure_directory,"breastfeeding_infant.png"), height = 6, units = "in", width = 10, res = image_resolution)
  print(temp_plot)
  dev.off()
    
  # Sutdy Site 
  temp_plot <- full_df %>% filter(cleaned_base_var  == "Study Site") %>% 
          # filter(cleaned_base_var == p) %>% 
          ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = chosen_level))+
          # geom_errorbar(aes(ymin = min_response, ymax = max_response), position = "dodge", size = 0.5)+
          # geom_violin(size = 2)+
          geom_boxplot(position = position_dodge(1))+
          coord_flip()+
          # scale_color_manual(values = color_generattor(15))+
          facet_grid(pathogen ~. , scales = "free_y")+
          theme(
            panel.background = element_blank(), 
            panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
            panel.grid.major.x = element_line(color = "gray90"), 
            panel.grid.major.y = element_line(color = "gray90"), 
            strip.text = element_blank(),
            strip.background = element_blank(),
            axis.text.x = element_text(angle = 30, vjust = 0), 
            plot.title = element_text(size = rel(2)),
            legend.text = element_text(size = rel(1.8)),
            axis.text.y = element_markdown(size = rel(1.8)),
          )+
          scale_color_manual(
            limits = c("Bangladesh (reference)", "Brazil", "India", "Nepal", "Pakistan", "Peru", "South Africa", "Tanzania"),
            values = c("#666666","#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d"),
            drop = F)+
          scale_x_discrete(drop = F)+
          labs(title = "Response to Study Site in Infant Model", 
               x = "", 
               y = "Coefficient Value", 
               color = "")
  png(file.path(figure_directory,"study_site_infant.png"), height = 12, units = "in", width = 10, res = image_resolution)
  print(temp_plot)
  dev.off()
  
  
  
  temp_plot <- full_df %>% filter(cleaned_base_var  == "Age (Timepoint)") %>% 
          # filter(cleaned_base_var == p) %>% 
          ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = chosen_level))+
          # geom_violin(size = 2)+
          geom_boxplot(position = position_dodge(1))+
          geom_hline(yintercept = 0, color = "gray40", size = 1.2, linetype = "dashed")+
          coord_flip()+
          # scale_color_manual(values = color_generattor(15))+
          scale_color_manual(
            limits = c("9 month", "12 month (reference)", "15 month"), 
            values = c("#7fc97f", "#666666", "#8da0cb")
          )+
          facet_grid(pathogen ~. , scales = "free_y")+
          theme(
            panel.background = element_blank(), 
            panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
            panel.grid.major.x = element_line(color = "gray90"), 
            panel.grid.major.y = element_line(color = "gray90"), 
            strip.text = element_blank(),
            strip.background = element_blank(),
            axis.text.x = element_text(), 
            plot.title = element_text(size = rel(2)),
            legend.text = element_text(size = rel(1.8)),
            axis.text.y = element_markdown(size = rel(1.8)),
          )+
          labs(title = "Response to Discrete Timepoint in Infant Model", 
               y = "Coefficient Value", x = "", 
               color = "")
  png(file.path(figure_directory,"timepoint_infant.png"), height = 15, units = "in", width = 10, res = image_resolution)
  print(temp_plot)
  dev.off()
  
}


generate_signed_response_infant(cleaned_infant_signed_response, graph_partitions())
```


```{r, newborn_1_occurences}
cleaned_newborn_signed_response %>%  group_by(pathogen, base_var) %>% mutate(n_parts = n_distinct(k_partition)) %>% 
  filter(n_parts == 10) %>%
  ungroup() %>% group_by(base_var) %>% mutate(n_paths = n_distinct(pathogen)) %>%
  filter(n_paths == 1) %>%
  mutate(base_var = gsub("\\[.+", "", base_var)) %>% 
  ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = chosen_level))+
  geom_violin(#aes(group = chosen_level), 
              #position = position_dodge(width = 0.5)
    size = 2
              )+
  geom_hline(yintercept = 0, color = "gray40", size = 1.2, linetype = "dashed")+
  coord_flip()+
  scale_color_manual(values = color_generattor(15))+
  theme(
    panel.background = element_blank(), 
    panel.border = element_rect(color = "gray40", fill = NA, size = 0.75),
    panel.grid.major.x = element_line(color = "gray90"), 
    panel.grid.major.y = element_line(color = "gray90"), 
    strip.text = element_text(angle = 0), 
    axis.text.x = element_text(angle = 30, vjust = 0)
  )+
  facet_wrap(.~base_var, ncol = 8,
             scale = "free_x"
             )+
  labs(title = "Newborn Model Variables in 1 pathogen")
```

```{r,  fig.height= 15, fig.width=20}
cleaned_infant_signed_response %>% group_by(pathogen, base_var) %>% mutate(n_parts = n_distinct(k_partition)) %>% 
  filter(n_parts == 10) %>%
  ungroup() %>% group_by(base_var) %>% mutate(n_paths = n_distinct(pathogen)) %>%
  filter(n_paths > 1) %>%
  mutate(base_var = gsub("\\[.+", "", base_var)) %>% 
  ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = chosen_level))+
  geom_violin(#aes(group = chosen_level), 
              #position = position_dodge(width = 0.5)
    size = 2
              )+
  geom_hline(yintercept = 0, color = "gray40", size = 1.2, linetype = "dashed")+
  coord_flip()+
  # scale_y_continuous(breaks = c(0))+
  # scale_x_discrete(sec.axis = dup_axis())+
  # scale_color_discrete()+
  scale_color_manual(values = color_generattor(15))+
  theme(
    panel.background = element_blank(), 
    panel.border = element_rect(color = "gray0", fill = NA, size = 0.75),
    panel.grid.major.x = element_line(color = "gray90"), 
    panel.grid.major.y = element_line(color = "gray90"), 
    strip.text = element_text(angle = 0), 
    axis.text.x = element_text(angle = 0, vjust = 0)
  )+
  facet_wrap(.~base_var, ncol = 6, 
             scale = "free_x"
             )+
  labs(title = "Infant Model with variables found greater in >1 pathogens")
```

```{r infant_one_pathogen, fig.height= 20, fig.width=30}
cleaned_infant_signed_response %>% group_by(pathogen, base_var) %>% mutate(n_parts = n_distinct(k_partition)) %>% 
  filter(n_parts == 10) %>%
  ungroup() %>% group_by(base_var) %>% mutate(n_paths = n_distinct(pathogen)) %>%
  filter(n_paths == 1) %>%
  mutate(base_var = gsub("\\[.+", "", base_var)) %>% 
  ggplot(aes(x = pathogen, y = as.numeric(signed_values), color = chosen_level))+
  geom_violin(#aes(group = chosen_level), 
              #position = position_dodge(width = 0.5)
    size = 2
              )+
  geom_hline(yintercept = 0, color = "gray40", size = 1.2, linetype = "dashed")+
  coord_flip()+
  # scale_y_continuous(breaks = c(0))+
  # scale_x_discrete(sec.axis = dup_axis())+
  # scale_color_discrete()+
  scale_color_manual(values = color_generattor(33))+
  theme(
    panel.background = element_blank(), 
    panel.border = element_rect(color = "gray0", fill = NA, size = 0.75),
    panel.grid.major.x = element_line(color = "gray90"), 
    panel.grid.major.y = element_line(color = "gray90"), 
    strip.text = element_text(angle = 0), 
    axis.text.x = element_text(angle = 0, vjust = 0)
  )+
  facet_wrap(.~base_var, ncol = 8, 
             scale = "free_x"
             )+
  labs(title = "Infant Model for variables that occur in 1 pathogen")
```

