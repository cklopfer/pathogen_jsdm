#' Archive old code, and we'll ignore this in gitignore 


find_targeted_env_response <- function(model, target_coef){
  pathogens <- names(model$trace$B)
  shared_response <- array(data = NA, dim = c(length(pathogens), length(pathogens), nrow(model$trace$B[[1]])))
  tracker <- upper.tri(shared_response[,,1], diag = TRUE)
  
  col_num <- which(colnames(model$call$X) == target_coef)
  cov_X <- cov(model$call$X)
  print(dim(shared_response))
  for(p1 in 1:length(pathogens)){
    for(p2 in 1:length(pathogens)){
      if(tracker[p1, p2]){
        
        path1_coef <- model$trace$B[[p1]][,target_coef]
        path2_coef <-  model$trace$B[[p2]][,target_coef]
        part_A <- (path1_coef * path2_coef)
        
        cov_subset <- matrix(data = 1, nrow = nrow(model$trace$B[[1]]), ncol = 1) %*% cov_X[col_num, -c(1, col_num)]
        
        part_b <- rowSums((part_A %*% matrix(data = 1, ncol = ncol(model$trace$B[[1]]) - 2, nrow = 1) + 
                             (model$trace$B[[p1]][,-c(1, col_num)] * model$trace$B[[p1]][,-c(1, col_num)])) * cov_subset)
        
        
        part_c <- (path1_coef^2 * rowSums(((path1_coef %*% matrix(data = 1, ncol = ncol(model$trace$B[[1]]) - 2,
                                                                  nrow = 1)) * model$trace$B[[p1]][,-c(1, col_num)] * cov_subset)^2))
        
        part_d <- (path2_coef^2 * rowSums(((path2_coef %*% matrix(data = 1,
                                                                  ncol = ncol(model$trace$B[[1]]) - 2,
                                                                  nrow = 1)) * model$trace$B[[p2]][,-c(1, col_num)] * cov_subset)^2))
        
        
        
        final <- (part_A * part_b)/ (2 * sqrt(part_c * part_d))
        
        shared_response[p1, p2,] <- final
        
      }
    }
  }
  return(shared_response)
}


find_targeted_env_response2 <- function(model, selected_env){
  pathogens <- names(model$trace$B)
  shared_response <- array(data = NA, dim = c(length(pathogens), length(pathogens), nrow(model$trace$B[[1]])))
  tracker <- upper.tri(shared_response[,,1], diag = TRUE)
  fail_cases <- vector(mode = "numeric", length = 26^2 * 1000)
  fail_cases[] <- NA
  
  for(p1 in 1:length(pathogens)){
    for(p2 in 1:length(pathogens)){
      # if(tracker[p1, p2]){
      part_A <- model$trace$B[[pathogens[p1]]][,selected_env] * model$trace$B[[pathogens[p2]]][,selected_env]
      part_B_sum <- 0
      part_C_sum <- 0 
      part_D_sum <- 0
      
      for(k in colnames(model$trace$B[[pathogens[p1]]])){
        if(k != "intercept" & k != selected_env){
          cov_X <- cov(model$call$X[,selected_env], model$call$X[,k])
          
          part_B_sum <- part_B_sum + ((model$trace$B[[p1]][,selected_env] * model$trace$B[[p2]][,selected_env] + 
                                         model$trace$B[[p1]][,k] * model$trace$B[[p2]][,k]) * cov_X)
          
          part_C_sum <- part_C_sum + 
            (model$trace$B[[p1]][,selected_env] * model$trace$B[[p1]][,k] * cov_X)
          
          part_D_sum <- part_D_sum + 
            (model$trace$B[[p2]][,selected_env] * model$trace$B[[p2]][,k] * cov_X)
          
        }
        
      }
      
      part_C <- model$trace$B[[p1]][,selected_env]^2 * part_C_sum
      part_D <- model$trace$B[[p2]][,selected_env]^2 * part_D_sum
      
      denominator <- part_C * part_D
      if(sum(denominator < 0) > 0){
        if(sum(is.na(fail_cases)) == length(fail_cases)){
          
          fail_cases[c(1:sum(denominator < 0, na.rm = T))] <- denominator[denominator < 0]
        }else{
          fail_cases[c(match(NA, fail_cases):sum(denominator < 0, na.rm = T))] <- denominator[denominator < 0]
          
        }
        
      }
      
      # shared_response[p1, p2,] <- (part_A * part_B_sum) / (2 * sqrt(abs(part_C * part_D)))
      shared_response[p1, p2,] <- (part_A * part_B_sum) #/ (2 * sqrt(abs(part_C * part_D)))
      if(sum(shared_response[p1,p2,]) < 0){
        print("Error, Cov Sum < 0")
      }
      
      # }
    }
  }
  after_conversion <- array(apply(shared_response, 3, FUN =  function(x) (cov2cor(x))), dim = dim(shared_response))
  return(list(before = shared_response, after = after_conversion, fails = fail_cases))
}

find_targeted_env_response3 <- function(model, selected_env){
  pathogens <- names(model$trace$B)
  
  shared_response <- array(data = NA, dim = c(length(pathogens), length(pathogens), nrow(model$trace$B[[1]])), 
                           dimnames = list(pathogens, pathogens, seq(nrow(model$trace$B[[1]]))))
  tracker <- upper.tri(shared_response[,,1], diag = TRUE)
  env_variables <- vector(mode = "numeric", length = length(pathogens))
  names(env_variables) <- names(model$trace$B)
  for(t in 1:nrow(model$trace$B[[1]])){
    # print(t)
    for(p1 in 1:length(pathogens)){
      env_variables[p1] <- model$trace$B[[p1]][t,selected_env]
    }
    linear_com_b <- model$call$X[,selected_env] %*% matrix(env_variables, nrow = 1)
    colnames(linear_com_b) <- names(env_variables)
    linear_comb <- cov(linear_com_b)
    shared_response[,,t] <- linear_comb
  }
  
  mean_m <- apply(shared_response, c(1,2), mean)
  flattened_array <- flatten_array(shared_response, pathogens)
  cis <- get_hdi_intervals(flattened_array)
  
  
  low_ci <- cis[[1]]
  high_ci <- cis[[2]]
  
  sig_m <- mean_m
  sig_m[which((mean_m < 0 & high_ci > 0) | (mean_m > 0 & low_ci < 0))] <- NA
  
  rownames(sig_m) <- rownames(mean_m) <- pathogens
  colnames(sig_m) <- colnames(mean_m) <- pathogens
  final <- list(
    'mean_m' = mean_m,
    # 'sd_m' = sd_m, 
    'low_m' = low_ci,
    'high_m' = high_ci,
    'flattened' = flattened_array,
    'sig_m' = sig_m, 
    'coef_df' <- do.call(rbind, lapply(seq(dim(shared_response)[3]), convert_slice_to_df, shared_response, pathogens) )
  )
  return(final)
}


test_shared_response <- find_targeted_env_response2(test_model, 'stool_type')

sample_shared_response <- find_targeted_env_response2(env_bayescom_chain[[1]], 'stool_type')

hist(log10(abs(sample_shared_response[[3]])), main = "Log_10(|Failed Denominators|)", xlab = "Log10(|Denomator Values < 0|)")


sample_shared_response <- find_targeted_env_response3(env_bayescom_chain[[1]], 'Tanzania')
# This is also not working, values are still greater than 1. 
# Not working, the values are all greater than 1. 

new_shared_response <- find_targeted_env_response(test_model, "stool_type")


#' Prep the dataset for use in the JSDM model
#' 
#' This is was the code run as of 03 May 21. 



source(file.path("scripts", "constants.R"), echo = F)


prep_earlylife_inv_df <- function(d){
  temp <- d %>%
    mutate(present_country = 1) %>%
    tidyr::pivot_wider(names_from = "country", values_from = "present_country", values_fill = 0) %>% select(-Brazil) %>%
    mutate(collection_tp_present = 1) %>% 
    tidyr::pivot_wider(names_from = "specific_partition", values_from = "collection_tp_present", values_fill = 0) %>% select(-`6 month`) %>% 
    mutate(improved_water = 1 - grepl("Improved", `Drinking water source [ENVO_00003064]`) * 1) %>% 
    mutate(stool_type = (stool_type == "Diarrhea") * 1) %>%
    mutate(urban = (`Urban or rural site [EUPATH_0011928]` == "Urban") * 1) %>%
    mutate(improved_toilet = 1 - grepl("Improved", `Toilet or latrine type [EUPATH_0011744]`) * 1) %>%
    mutate(breast_feeding_only = 1 - (`Breastfeeding status [EUPATH_0011048]` == "Exclusive breastfeeding") * 1) %>%
    rename(
      maternal_education = `Maternal education (years) [EUPATH_0011605]`) %>% 
    mutate(maternal_education = scale(maternal_education)) %>% 
    select(-observation_id, -`Drinking water source [ENVO_00003064]`, -age, -study,
           -`Urban or rural site [EUPATH_0011928]`, -`Toilet or latrine type [EUPATH_0011744]`,
           -`Breastfeeding status [EUPATH_0011048]`) %>% ungroup()
  
  final <- left_join(
    temp %>% select(-maternal_education) %>% group_by(participant, `1 month`, `3 month`) %>% summarise_all(sum) %>% mutate_all(~ ((.x > 0)*1)), 
    temp %>% select(participant, `1 month`, `3 month`, maternal_education) %>% group_by(participant, `1 month`, `3 month`) %>% 
      summarise_all(mean)) %>% ungroup() %>% select(-participant)
  return(final)
}

prep_latelife_env_df <- function(d){
  temp <- d %>%
    mutate(present_country = 1) %>%
    tidyr::pivot_wider(names_from = "country", values_from = "present_country", values_fill = 0) %>% select(-Brazil) %>%
    mutate(collection_tp_present = 1) %>% 
    tidyr::pivot_wider(names_from = "specific_partition", values_from = "collection_tp_present", values_fill = 0) %>% select(-`15 month`) %>% 
    mutate(improved_water = 1 - grepl("Improved", `Drinking water source [ENVO_00003064]`) * 1) %>% 
    mutate(stool_type = (stool_type == "Diarrhea") * 1) %>%
    mutate(urban = (`Urban or rural site [EUPATH_0011928]` == "Urban") * 1) %>%
    mutate(improved_toilet = 1 - grepl("Improved", `Toilet or latrine type [EUPATH_0011744]`) * 1) %>%
    mutate(breast_feeding_only = (`Breastfeeding status [EUPATH_0011048]` == "Exclusive breastfeeding") * 1) %>% # I'll define variables: This should be flipped. 
    rename(
      maternal_education = `Maternal education (years) [EUPATH_0011605]`) %>% 
    mutate(maternal_education = scale(maternal_education)) %>% 
    select(-observation_id, -`Drinking water source [ENVO_00003064]`, -age, -study,
           -`Urban or rural site [EUPATH_0011928]`, -`Toilet or latrine type [EUPATH_0011744]`,
           -`Breastfeeding status [EUPATH_0011048]`) %>% ungroup()
  
  nutrient_data <- temp %>% select(participant,  `9 month`, `12 month`,  contains("Daily")) %>%  
    rename(
      zinc = `Daily zinc (mg) [EUPATH_0011121]`, 
      iron = `Daily iron from all sources (mg) [EUPATH_0011093]`,
      vit_b12 = `Daily vitamin B12 (ug) [EUPATH_0011114]`, 
      vit_A = `Daily vitamin A (ug) [EUPATH_0011113]`, 
      calcium = `Daily calcium (mg) [EUPATH_0011084]`
    ) %>% group_by(participant,  `9 month`, `12 month`) %>% 
    summarise(across(everything(), mean)) %>% ungroup() %>% 
    mutate(across(-one_of("participant", "9 month", "12 month"), scale)) %>% ungroup()
  
  
  final <- left_join(
    temp %>% select(-maternal_education, -contains("Daily")) %>% group_by(participant, `9 month`, `12 month`) %>% summarise_all(sum) %>% 
      group_by(participant, `9 month`, `12 month`) %>% mutate_all(~ ((.x > 0)*1)), 
    temp %>% select(participant, `9 month`, `12 month`, maternal_education) %>% group_by(participant, `9 month`, `12 month`) %>% 
      summarise_all(mean) %>% ungroup()) %>% ungroup()
  
  final <- left_join(
    final, 
    nutrient_data,
    suffix = c("", "")) %>%ungroup() %>% select(-participant)
  
  return(final)
}

timeseries_subset_early_life <- function(early_df){
  ts_only <-  early_df %>% mutate(study = 'maled') %>% partition_timepoints(14, 14) %>% filter(specific_partition != stool_type) %>% 
    prep_earlylife_inv_df()
  
  selected_pathogens <- ts_only %>% tidyr::pivot_longer(cols = get_pathogens(), names_to = "pathogens", values_to = "present") %>% 
    group_by(pathogens) %>% summarise(count = sum(present)) %>%  filter(count > 5) %>% select(pathogens) %>% unlist(use.names = F)
  
  early_PA <- ts_only %>% select(selected_pathogens)
  early_env <- ts_only %>% select(-get_pathogens())
  
  return(list(early_PA, early_env))
}

timeseries_subset_late_life <- function(late_df){
  ts_only <-  late_df %>% mutate(study = 'maled') %>% partition_timepoints(14, 14) %>% filter(specific_partition != stool_type) %>% 
    prep_latelife_env_df()
  
  selected_pathogens <- ts_only %>% tidyr::pivot_longer(cols = get_pathogens(), names_to = "pathogens", values_to = "present") %>% 
    group_by(pathogens) %>% summarise(count = sum(present)) %>%  filter(count > 5) %>% select(pathogens) %>% unlist(use.names = F)
  
  early_PA <- ts_only %>% select(selected_pathogens)
  early_env <- ts_only %>% select(-one_of(get_pathogens())) 
  
  return(list(early_PA, early_env))
}



# partition_timepoints <- function(d_f, maled_window = 7, provide_window= 7){
#   maled_diar_window <- 15
#   provide_diar_window <- 15
#   
#   return(d_f %>% mutate(specific_partition = case_when(
#     stool_type == "Asymptomatic" & study == "provide" & 
#       (age >= 42 - (provide_window - 7) & age <= 42 + (provide_window + 7)) ~ "1 month", 
#     stool_type == "Asymptomatic" & study == "provide" & 
#       (age >= 105 - (provide_window - 7) & age <= 105 + (provide_window + 7)) ~ "3 month", 
#     stool_type == "Asymptomatic" & study == "provide" & 
#       (age >= 168 - (provide_window - 7)  & age <= 168 + (provide_window + 7)) ~ "6 month",
#     
#     stool_type != "Asymptomatic" & study == "provide" & 
#       (age >= 42 - (provide_diar_window) & age <= 42 + (provide_diar_window)) ~ "1 month", 
#     stool_type != "Asymptomatic" & study == "provide" & 
#       (age >= 105 - (provide_diar_window) & age <= 105 + (provide_diar_window)) ~ "3 month", 
#     stool_type != "Asymptomatic" & study == "provide" & 
#       (age >= 168 - (provide_diar_window)  & age <= 168 + (provide_diar_window)) ~ "6 month",
#     
#     stool_type == "Asymptomatic" & study == "maled" & (age >= 28 - (maled_window - 7) & age <= 28 + maled_window) ~ "1 month", 
#     stool_type == "Asymptomatic" & study == "maled" & (age >= 91 - (maled_window - 7) & age <= 91 + maled_window) ~ "3 month", 
#     stool_type == "Asymptomatic" & study == "maled" & (age >= 182 - (maled_window - 7) & age <= 182 + maled_window) ~ "6 month", 
#     
#     stool_type != "Asymptomatic" & study == "maled" & (age >= 28 - (maled_diar_window) & age <= 28 + maled_diar_window) ~ "1 month", 
#     stool_type != "Asymptomatic" & study == "maled" & (age >= 91 - (maled_diar_window) & age <= 91 + maled_diar_window) ~ "3 month", 
#     stool_type != "Asymptomatic" & study == "maled" & (age >= 182 - (maled_diar_window) & age <= 182 + maled_diar_window) ~ "6 month", 
#     TRUE ~ stool_type
#   )) )
#   
# }

partition_timepoints <- function(d_f, maled_window = 7, provide_window= 7){
  maled_diar_window <- 15
  provide_diar_window <- 15
  
  return(d_f %>% mutate(specific_partition = case_when(
    stool_type == "Asymptomatic" & study == "provide" & 
      (age >= 42 - (provide_window - 7) & age <= 42 + (provide_window + 7)) ~ "1 month", 
    stool_type == "Asymptomatic" & study == "provide" & 
      (age >= 105 - (provide_window - 7) & age <= 105 + (provide_window + 7)) ~ "3 month", 
    stool_type == "Asymptomatic" & study == "provide" & 
      (age >= 168 - (provide_window - 7)  & age <= 168 + (provide_window + 7)) ~ "6 month",
    
    stool_type != "Asymptomatic" & study == "provide" & 
      (age >= 42 - (provide_diar_window) & age <= 42 + (provide_diar_window)) ~ "1 month", 
    stool_type != "Asymptomatic" & study == "provide" & 
      (age >= 105 - (provide_diar_window) & age <= 105 + (provide_diar_window)) ~ "3 month", 
    stool_type != "Asymptomatic" & study == "provide" & 
      (age >= 168 - (provide_diar_window)  & age <= 168 + (provide_diar_window)) ~ "6 month",
    
    stool_type == "Asymptomatic" & study == "maled" & (age >= 28 - (maled_window - 7) & age <= 28 + maled_window) ~ "1 month", 
    stool_type == "Asymptomatic" & study == "maled" & (age >= 91 - (maled_window - 7) & age <= 91 + maled_window) ~ "3 month", 
    stool_type == "Asymptomatic" & study == "maled" & (age >= 182 - (maled_window - 7) & age <= 182 + maled_window) ~ "6 month", 
    stool_type == "Asymptomatic" & study == "maled" & (age >= 266 - (maled_window - 7) & age <= 266 + maled_window) ~ "9 month", 
    stool_type == "Asymptomatic" & study == "maled" & (age >= 336 - (maled_window - 7) & age <= 336 + maled_window) ~ "12 month",
    stool_type == "Asymptomatic" & study == "maled" & (age >= 420 - (maled_window - 7) & age <= 420 + maled_window) ~ "15 month", 
    
    stool_type != "Asymptomatic" & study == "maled" & (age >= 28 - (maled_diar_window) & age <= 28 + maled_diar_window) ~ "1 month", 
    stool_type != "Asymptomatic" & study == "maled" & (age >= 91 - (maled_diar_window) & age <= 91 + maled_diar_window) ~ "3 month", 
    stool_type != "Asymptomatic" & study == "maled" & (age >= 182 - (maled_diar_window) & age <= 182 + maled_diar_window) ~ "6 month",
    stool_type != "Asymptomatic" & study == "maled" & (age >= 266 - (maled_diar_window - 7) & age <= 266 + maled_diar_window) ~ "9 month", 
    stool_type != "Asymptomatic" & study == "maled" & (age >= 336 - (maled_diar_window - 7) & age <= 336 + maled_diar_window) ~ "12 month",
    stool_type != "Asymptomatic" & study == "maled" & (age >= 420 - (maled_diar_window - 7) & age <= 420 + maled_diar_window) ~ "15 month", 
    TRUE ~ stool_type
  )) )
  
}
