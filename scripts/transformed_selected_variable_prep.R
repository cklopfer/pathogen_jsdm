#' The selected variables 
library(dplyr)

source(file.path("scripts", "transferred_functions.R"), echo = F)
source(file.path("scripts", "constants.R"), echo = F)


get_newborn_env_variables <- function(){
  
  newborn_vars <- list(
    "continuous" = c(
      "Age 1st solid food given (days) ",
      "Cumulative ALRI episode count ",
      "Cumulative days within diarrheal episodes ",
      "Loose stools in day, caregiver report ",
      "Family income (local currency) "
    ), 
    "binary" = c(
      "Chair ",
      "Dairy reported at last visit ",
      "Fever, caregiver report ",
      "Infant formula ",
      "Table ",
      # "Use of antibiotics, caregiver report " , 
      "Prelacteal feeding ",
      "improved_waste_disposal", 
      "wet_season"
      
    ), 
    "category" =c(
      "Appetite, caregiver report ",
      "breastfed_status_combined",
      "country",
      "Sex ",
      "specific_partition",
      "stool_type"
    )
  )
}

get_infant_env_variables <- function(){
  
  infant_vars <- list(
    "continuous" = c(
      "Age 1st solid food given (days) ",
      "Age last exclusively breastfed (days) ",
      "Daily sugars (g) ",
      # "Daily vitamin B6 (mg) ",
      # "Family income (local currency) ",
      "Fish or shellfish count ",
      "Meat count ",
      "Myeloperoxidase (ng/mL) "
      # "Cumulative ALRI episode count ",            
      # "Cumulative days within diarrheal episodes ",
      # "Loose stools in day, caregiver report "    
      
    ), 
    "binary" = c(
      # "Chair ",
      # "Dairy reported at last visit ",
      "Fever, caregiver report ",
      # "Infant formula ",                           
      # "Table ",                             
      # "Use of antibiotics, caregiver report " , 
      "ORT administered, caregiver report ", 
      "Refrigerator ", 
      "Stool blood present ", 
      "wet_season"
    ), 
    "category" =c(
      # "Appetite, caregiver report ",
      "breastfed_status_combined",
      "country",
      "Sex ",
      "specific_partition", 
      "stool_type"
    )
  )
}

get_reference_variables <- function(){
  ref_vars <- c(
    "Appetite, caregiver report ___ Normal appetite", 
    # "Appetite, caregiver report ___ Normal appetite"
    "breastfed_status_combined_not",
    "country___ Brazil", 
    "Sex ___ Female",
    "specific_partition___ 1 month", 
    "specific_partition___ 9 month",
    "stool_type___ Asymptomatic"
  )
  # final <- sapply(ref_vars, function(.x) paste("__", .x))
  # final <- unname(final)
  return(ref_vars)
}


transform_data <- function(d_f, type){
  reference_variables <- get_reference_variables()
  if(type == "newborn"){
    variable_selection <- get_newborn_env_variables()
  }else if(type == "infant"){
    variable_selection <- get_infant_env_variables()
  }
  names(d_f) <- gsub("\\[.*\\]", "", names(d_f))
  transformed <- d_f %>% select(all_of(c(variable_selection[[1]], variable_selection[[2]], variable_selection[[3]], "k_partition"))) %>% 
    mutate(temp_ID = seq(nrow(.)))
  for(n in variable_selection[['category']]){
    levels_n <- unique(d_f[[n]])
    transformed <- transformed %>% mutate(var_value = 1) %>% 
      tidyr::pivot_wider(names_from = n, values_from = "var_value", names_prefix = paste0(n, "_"), values_fill = 0)#%>% 
      # select(-one_of(paste0(paste0(n, "_"), levels_n[length(levels_n)])))
    # print(paste0("Dropped: ", paste0(paste0(n, "_"), levels_n[length(levels_n)])))
  }
  final <- transformed %>% select(-any_of(reference_variables)) %>% 
    group_by(k_partition) %>% mutate(across(all_of(variable_selection[["continuous"]]), scale)) %>% select(-temp_ID)
  return(final)
}


only_populous_pathogens <- function(d_f){
  n_partitions <- length(unique(d_f$k_partition))
  pathogens <- d_f %>% select(any_of(c(get_pathogens(), "k_partition")))
  target_pathogens <- pathogens %>% group_by(k_partition) %>% 
    summarise(across(-any_of("k_partition"), sum)) %>% ungroup() %>% 
    tidyr::pivot_longer(-any_of("k_partition"), 
                        names_to = "pathogen", 
                        values_to = "total_present") %>% group_by(pathogen) %>% 
    summarise(abundance_filter = sum(total_present > 5)) %>% 
    filter(abundance_filter/n_partitions == 1) %>% pull(pathogen)
  print(target_pathogens)
  final <- d_f %>% select(any_of(c(target_pathogens)))
  return(final)
}


prep_dataset <- function(full_data, model_version){
  transformed <- full_data %>% transform_data(model_version)
  final <- cbind(full_data %>% only_populous_pathogens(), 
                 transformed)
  return(final)
}

newborn_split_dataset <- readr::read_csv(file.path("data", paste("newborn", "variable_selection_training_crossval.csv", sep = "_"))) %>% 
  combine_like_pathogens()

infant_split_dataset <- readr::read_csv(file.path("data", paste("infant", "variable_selection_training_crossval.csv", sep = "_"))) %>% 
  combine_like_pathogens()

newborn_crossval_tofile <- prep_dataset(newborn_split_dataset, "newborn")
write.csv(newborn_crossval_tofile, file = file.path("data", "newborn_selected_transformed_variables_crossval.csv"), row.names = F)

infant_crossval_tofile <- prep_dataset(infant_split_dataset, "infant")
write.csv(infant_crossval_tofile, file = file.path("data", "infant_selected_transformed_variables_crossval.csv"), row.names = F)



newborn_training_set <- readr::read_csv(file.path("data", paste("newborn", "variable_selection_training_all.csv", sep = "_"))) %>% 
  combine_like_pathogens()

infant_training_set <- readr::read_csv(file.path("data", paste("infant", "variable_selection_training_all.csv", sep = "_"))) %>% 
  combine_like_pathogens()


newborn_tofile <- prep_dataset(newborn_training_set %>% mutate(k_partition = 1), "newborn") %>% 
  select(names(newborn_crossval_tofile)) %>% select(-k_partition)
write.csv(newborn_tofile, file = file.path("data", "newborn_selected_transformed_variables_all.csv"), row.names = F)

infant_tofile <- prep_dataset(infant_training_set %>% mutate(k_partition = 1), "infant") %>% 
  select(names(infant_crossval_tofile)) %>% select(-k_partition)
write.csv(infant_tofile, file = file.path("data", "infant_selected_transformed_variables_all.csv"), row.names = F)
