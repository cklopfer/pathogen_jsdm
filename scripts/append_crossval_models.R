#' Append Bayescomm models together

library(dplyr)


model_directory <- "crossval_TEST"


get_posterior_data <- function(dir_name){
  parent_dir <- file.path("models", "crossval_model", dir_name)
  writing_directory <- file.path("models", "crossval_model")
  file_loop <- list.files(parent_dir)
  
  all_r_posteriors <- list()
  all_b_posteriors <- list()
  counter <- 1
  for(f in file_loop){
    if(stringr::str_detect(f, "crossval")){
      partitioned_directory <- stringr::str_split(f, "\\_")[[1]]
      k <- as.numeric(partitioned_directory[2])
      for(f2 in list.files(file.path(parent_dir, f))){
        model_type <- f2
        for(f3 in list.files(file.path(parent_dir, f, f2))){
          chain_num <- stringr::str_extract(f3, "[0-9]+")
          imported_model <- readRDS(file.path(parent_dir, f, f2, f3))
          extracted_posterior <- extract_posterior(imported_model)
          all_b_posteriors[[counter]] <- extracted_posterior[[1]] %>% mutate(k_partition = k, type = model_type, chain_n = chain_num)
          all_r_posteriors[[counter]] <- extracted_posterior[[2]] %>% mutate(k_partition = k, type = model_type, chain_n = chain_num)
          counter <- counter + 1
        }
      }
    }
    
  }
  all_b_df <- do.call(bind_rows, all_b_posteriors)
  all_r_df <- do.call(bind_rows, all_r_posteriors)
  write.csv(all_b_df, file.path(writing_directory, paste0(dir_name, "_b_posterior.csv")), row.names = FALSE)
  write.csv(all_r_df, file.path(writing_directory, paste0(dir_name, "_r_posterior.csv")), row.names = FALSE)
  
  return(list(all_b_df, all_r_df))
}

extract_posterior <- function(bayescomm_model){
  b_posterior <- do.call(rbind, bayescomm_model$trace$B) %>% 
    as.data.frame() %>% 
    mutate(pathogen = rep(names(bayescomm_model$trace$R), 
                          each = dim(bayescomm_model$trace$B[[1]])[1]))
  r_posterior <- bayescomm_model$trace$R %>% as.data.frame()
  return(list(b_posterior, r_posterior))
}


get_posterior_data(model_directory)




