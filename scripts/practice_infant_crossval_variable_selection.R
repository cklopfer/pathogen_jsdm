#' Running Parallel model selection. 
#' 
#' We'll do step-wise regression for variable selection on the MAL-ED dataset, 
#' but we'll do it con cross-validation of the training dataset. 
#' 
#' 
#' NOTE: Need to run from project directory 
#' 
library(dplyr)
library(foreach)
# library(parallel)
library(doSNOW)

source(file.path("scripts", "constants.R"), echo = F)
source(file.path("scripts", "transferred_functions.R"), echo = F)


# CONSTANTS 

k_fold <- 2
n_steps <- 10 # default is 1000

model_type <- "infant"

ncores <- parallel::detectCores() / 2

# Import the SPLIT training dataset. This keeps the dataset consistent for testing. 
# - This will have a variable that indicates the split of that dataset. 
split_dataset <- readr::read_csv(file.path("data", paste(model_type, "variable_selection_training_crossval.csv", sep = "_")))

n_pathogens <- split_dataset %>% select(get_pathogens()) %>% combine_like_pathogens() %>%   ncol() 

pathogen_names <- split_dataset %>% select(get_pathogens()) %>% combine_like_pathogens() %>% names()



# Run the crossvalidation in parallel, one loop for each variable and one for each pathogen. 
# tryCatch({
clust <- makeCluster(ncores)
registerDoSNOW(clust)

# p <- 1
# k <- 1
idx <- 1
all_pathogen_selection <- list()

# all_pathogen_selection <- foreach(k = seq(k_fold), .combine = 'rbind', .verbose = T)%:% foreach(p = seq(n_pathogens), .combine = 'rbind', .verbose = T) %dopar% {
for(k in 1:k_fold){
  for (p in 1:n_pathogens){
    # library(dplyr)
    
    # cross_val_directory <- file.path("models", "crossval_model", EXP_NAME, paste("crossval", k, sep = "_"))
    
    # Get the subset of the dataframe for that k crossvalidation. 
    k_data_subset <- split_dataset %>% filter(k_partition == k) %>% select(-k_partition)
    
    # SPlit the Dataset into PA and ENV data. 
    k_subset_PA <- k_data_subset %>% select(get_pathogens()) %>% combine_like_pathogens()
    k_subset_env <- k_data_subset %>% select(-any_of(get_pathogens()))
    
    k_subset_env <- k_subset_env[which(lapply(k_subset_env, function(x) length(unique(x))) > 1)]
    
    variable_selections <- matrix(0, nrow = 1, ncol = ncol(k_subset_env))
    colnames(variable_selections) <- names(k_subset_env)
    rownames(variable_selections) <- names(k_subset_PA)[p]
    
    minimum_size <- glm(k_subset_PA[,p] ~ 1, data = k_subset_env, family = binomial(link = "probit"))
    
    maximum_size <- glm(k_subset_PA[,p] ~ ., data = k_subset_env, family = binomial(link = "probit"))
    
    selected_model <- step(minimum_size, formula(maximum_size), direction = "forward", steps = n_steps)
    
    selected_variables <- names(selected_model$coefficients)
    variable_tally <- (1:length(selected_variables))[-grep('\\(', selected_variables)]
    
    selected_variables <- gsub("\`", "", selected_variables)
    
    fixed_categorical <- unique(gsub("__.*$", "", selected_variables[variable_tally]))
    
    # Update the variable selection matrix
    variable_selections[1, fixed_categorical] <- 1
    
    variable_selections <- cbind(variable_selections, did_converge = selected_model$converged * 1, k_partition = k, pathogen_num = p)
    
    all_pathogen_selection[[idx]] <- variable_selections
  }
}
# }

# })

stopCluster(clust)

final <- do.call(rbind, all_pathogen_selection)

# Append the data table across all pathogens across all cross validation partitions. 
write.csv(final, file.path("data", "variable_selection", "infant_selection.csv"), row.names = T)


# Save to file. 