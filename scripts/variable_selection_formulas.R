# Fornula's for stepwise regression 


newborn_terminal_formula <- function(df_PA, df_env, path_num){
  return(
    glm(df_PA[,p] ~ factor(`stool_type`)                                                              +
          # `age`                                                                     +
          factor(`country`)                                                                 +
          # factor(`Stool consistency [EUPATH_0010217]`)                                      +
        #   `Cumulative sum aaiC-pos and aatA-pos stools [EUPATH_0010596]`            +
        #   `Cumulative sum Adenovirus-pos stools [EUPATH_0010600]`                   +
        #   `Cumulative sum Aeromonas-pos stools [EUPATH_0010604]`                    +
        #   `Cumulative sum Ascaris lumbricoides-pos stools [EUPATH_0010608]`         +
        #   `Cumulative sum Astrovirus-pos stools [EUPATH_0010612]`                   +
        #   # `Cumulative sum Trichuris trichiura-pos stools [EUPATH_0010616]`          +
        #   `Cumulative sum atypical EPEC-pos stools [EUPATH_0010620]`                +
        #   # `Cumulative sum Balantidium coli-pos stools [EUPATH_0010624]`             +
        #   `Cumulative sum Campylobacter-pos stools, by bacteriology [EUPATH_0010640]`+
        # # `Cumulative sum Chilomastix mesnili-pos stools [EUPATH_0010644]`          +
        #   `Cumulative sum Cryptosporidium-pos stools, by ELISA [EUPATH_0010648]`    +
        #   # `Cumulative sum Cyclospora-pos stools [EUPATH_0010652]`                   +
        #   `Cumulative sum EAEC-pos stools [EUPATH_0010656]`                         +
        #   `Cumulative sum aaiC-pos stools [EUPATH_0010660]`                         +
        #   `Cumulative sum aatA-pos stools [EUPATH_0010664]`                         +
        #   `Cumulative sum bfpA-pos stools [EUPATH_0010668]`                         +
        #   `Cumulative sum eae-pos stools [EUPATH_0010672]`                          +
        #   `Cumulative sum ETEC LT-pos stools [EUPATH_0010680]`                      +
        #   `Cumulative sum ETEC ST-pos stools [EUPATH_0010684]`                      +
        #   `Cumulative sum stx1-pos stools [EUPATH_0010688]`                         +
        #   `Cumulative sum stx2-pos stools [EUPATH_0010692]`                         +
        #   # `Cumulative sum Entamoeba histolytica-pos stools [EUPATH_0010696]`        +
        #   `Cumulative sum EIEC-pos stools [EUPATH_0010700]`                         +
        #   `Cumulative sum Endolimax nana-pos stools [EUPATH_0010704]`               +
        #   `Cumulative sum EPEC-pos stools [EUPATH_0010708]`                         +
        #   `Cumulative sum ETEC-pos stools [EUPATH_0010712]`                         +
        #   # `Cumulative sum Enterobius vermicularis-pos stools [EUPATH_0010716]`      +
        #   `Cumulative sum Giardia-pos stools [EUPATH_0010723]`                      +
        #   # `Cumulative sum Hymenolepis diminuta-pos stools [EUPATH_0010727]`         +
        #   # `Cumulative sum Hymenolepis nana-pos stools [EUPATH_0010731]`             +
        #   # `Cumulative sum Hookworm-pos stools [EUPATH_0010735]`                     +
        #   `Cumulative sum Iodamoeba butschlii-pos stools [EUPATH_0010739]`          +
        #   # `Cumulative sum Isospora-pos stools [EUPATH_0010743]`                     +
        #   `Cumulative sum ETEC LT-pos ST-neg stools [EUPATH_0010747]`               +
        #   `Cumulative sum Cryptosporidium-pos stools, by microscopy [EUPATH_0010751]`+
        #   # `Cumulative sum Entamoeba coli-pos stools [EUPATH_0010755]`               +
        #   `Cumulative sum Norovirus-pos stools [EUPATH_0010768]`                    +
        #   `Cumulative sum Norovirus GI-pos stools [EUPATH_0010772]`                 +
        #   `Cumulative sum Norovirus GII-pos stools [EUPATH_0010776]`                +
        #   `Cumulative sum Plesiomonas shigelloides-pos stools [EUPATH_0010790]`     +
        #   `Cumulative sum Rotavirus-pos stools [EUPATH_0010794]`                    +
        #   `Cumulative sum Salmonella-pos stools [EUPATH_0010798]`                   +
        #   # `Cumulative sum Schistosoma-pos stools [EUPATH_0010802]`                  +
        #   `Cumulative sum Shigella-pos stools [EUPATH_0010806]`                     +
        #   # `Cumulative sum Strongyloides stercoralis-pos stools [EUPATH_0010810]`    +
        #   `Cumulative sum STEC-pos stools [EUPATH_0010818]`                         +
        #   # `Cumulative sum Taenia-pos stools [EUPATH_0010822]`                       +
        #   `Cumulative sum Vibrio-pos stools [EUPATH_0010826]`                       +
        #   `Cumulative sum Yersinia enterocolitica-pos stools [EUPATH_0010830]`      +
          factor(`Stool blood present [EUPATH_0015217]`)                                    +
          # factor(`Study-defined diarrhea [EUPATH_0000665]`)                                +
          `Cumulative diarrheal episode count [EUPATH_0010478]`                     +
          `Cumulative days within diarrheal episodes [EUPATH_0010480]`              +
          factor(`ALRI episode status [EUPATH_0010507]`)                                    +
          `Cumulative ALRI episode count [EUPATH_0010516]`                          +
          # factor(`Moderate-to-severe diarrhea by GEMS criteria [EUPATH_0010524]`)           +
          # factor(`Any illness, caregiver report [EUPATH_0010525]`)                          +
          factor(`Activity level, caregiver report [EUPATH_0010526]`)                       +
          factor(`Appetite, caregiver report [EUPATH_0010527]`)                             +
          factor(`Vomiting, caregiver report [EUPATH_0010528]`)                           +
          factor(`Ear pain or pulling, caregiver report [EUPATH_0010529]`)                  +
          factor(`Use of antibiotics, caregiver report [EUPATH_0010530]`)                   +
          `Loose stools in day, caregiver report [EUPATH_0010533]`                  +
          factor(`Blood in stool, caregiver report [EUPATH_0010534]`)                       +
          factor(`ORT administered, caregiver report [EUPATH_0010536]`)                     +
          factor(`Cough, caregiver report [EUPATH_0010538]`)                                +
          factor(`Shortness of breath, caregiver report [EUPATH_0010539]`)                  +
          factor(`Fever, caregiver report [EUPATH_0010544]`)                                +
          factor(`Hospitalized [EUPATH_0010995]`)                                           +
          # `Cumulative days exclusively breastfed [EUPATH_0011014]`                  +
          # `Cumulative days not breastfed [EUPATH_0011015]`                          +
          # `Cumulative days partially breastfed [EUPATH_0011017]`                    +
          # `Cumulative days predominantly breastfed [EUPATH_0011019]`                +
          factor(`Dairy reported at last visit [EUPATH_0011020]`)                           +
          # factor(`Exclusively breastfed at last visit [EUPATH_0011023]`)                    +
          # factor(`Not breastfed at last visit [EUPATH_0011041]`)                            +
          # factor(`Partially breastfed at last visit [EUPATH_0011043]`)                      +
          # factor(`Predominantly breastfed at last visit [EUPATH_0011046]`)                 +
          # factor(`Breastfeeding status [EUPATH_0011048]`)                                   +
          `Animal milk or formula feedings yesterday [EUPATH_0011641]`              +
          # factor(`Active breastfeeding [EUPATH_0011645]`)                                   +
          # `Breastfeedings yesterday, daytime [EUPATH_0011646]`                      +
          # `Breastfeedings yesterday, nighttime [EUPATH_0011647]`                    +
          # `Formula feedings yesterday, daytime [EUPATH_0011656]`                    +
          # `Formula feedings yesterday, nighttime [EUPATH_0011657]`                  +
          factor(`Infant formula [FOODON_03303130]`)                                        +
          `Age 1st animal milk or formula given (days) [EUPATH_0011030]`            +
          factor(`Breastfed within 1st 24hrs [EUPATH_0011009]`)                             +
          factor(`Time between childbirth and 1st breastfeeding [EUPATH_0011010]`)          +
          factor(`Fed colostrum [EUPATH_0011011]`)                                          +
          factor(`Prelacteal feeding [EUPATH_0011012]`)                                     +
          `Age 1st animal milk or solids given (days) [EUPATH_0011033]`             +
          `Age 1st solid food given (days) [EUPATH_0011036]`                        +
          `Age last exclusively breastfed (days) [EUPATH_0011049]`                  +
          factor(`Sex [PATO_0000047]`)                                                      +
          factor(`Bed [ENVO_00000501]`)                                                     +
          # factor(`Drinking water source [ENVO_00003064]`)                                   +
          factor(`Television [ENVO_01000579]`)                                              +
          factor(`Refrigerator [ENVO_01000583]`)                                            +
          factor(`Table [ENVO_01000584]`)                                                   +
          factor(`Chair [ENVO_01000586]`)                                                   +
          factor(`Roof material [EUPATH_0000003]`)                                          +
          factor(`Floor material [EUPATH_0000006]`)                                         +
          factor(`Wall material [EUPATH_0000009]`)                                          +
          factor(`Bank account [EUPATH_0000167]`)                                           +
          `Persons sleeping in dwelling [EUPATH_0000714]`                           +
          `Family income (local currency) [EUPATH_0000727]`                         +
          factor(`Went 24hrs without food last month [EUPATH_0011135]`)                     +
          factor(`Ate foods not wanted last month [EUPATH_0011136]`)                        +
          factor(`Not able to eat preferred foods last month [EUPATH_0011137]`)             +
          factor(`Ate fewer meals due to lack of food last month [EUPATH_0011138]`)         +
          factor(`Went to sleep hungry last month [EUPATH_0011139]`)                        +
          factor(`Had to eat a limited variety of foods last month [EUPATH_0011141]`)       +
          factor(`No food of any kind last month [EUPATH_0011142]`)                         +
          factor(`Worried that there was not enough food last month [EUPATH_0011143]`)      +
          factor(`Ate smaller meal than needed last month [EUPATH_0011144]`)                +
          factor(`Household Food Insecurity Access Scale (HFIAS) [EUPATH_0011145]`)         +
          # factor(`Toilet or latrine type [EUPATH_0011744]`)                                 +
          factor(`Urban or rural site [EUPATH_0011928]`)                                    +
          factor(`specific_partition`) + 
          factor(breastfed_status_combined) +
          factor(improved_drinking_water) + 
          factor(improved_waste_disposal) + 
          factor(wet_season),
        
        data = df_env, family = binomial(link = "probit"),
        )
  )
}

infant_terminal_formula <- function(df_PA, df_env, path_num){
  return(
    glm(df_PA[,p] ~ factor(`stool_type`)                                                              +
          # `age`                                                                     +
          factor(`country`)                                                                 +
          # factor(`Stool consistency [EUPATH_0010217]`)                                      +
          # `Cumulative sum aaiC-pos and aatA-pos stools [EUPATH_0010596]`            +
          # `Cumulative sum Adenovirus-pos stools [EUPATH_0010600]`                   +
          # `Cumulative sum Aeromonas-pos stools [EUPATH_0010604]`                    +
          # `Cumulative sum Ascaris lumbricoides-pos stools [EUPATH_0010608]`         +
          # `Cumulative sum Astrovirus-pos stools [EUPATH_0010612]`                   +
          # `Cumulative sum Trichuris trichiura-pos stools [EUPATH_0010616]`          +
          # `Cumulative sum atypical EPEC-pos stools [EUPATH_0010620]`                +
          # # `Cumulative sum Balantidium coli-pos stools [EUPATH_0010624]`             +
          # `Cumulative sum Campylobacter-pos stools, by bacteriology [EUPATH_0010640]`+
          # # `Cumulative sum Chilomastix mesnili-pos stools [EUPATH_0010644]`          +
          # `Cumulative sum Cryptosporidium-pos stools, by ELISA [EUPATH_0010648]`    +
          # `Cumulative sum Cyclospora-pos stools [EUPATH_0010652]`                   +
          # `Cumulative sum EAEC-pos stools [EUPATH_0010656]`                         +
          # `Cumulative sum aaiC-pos stools [EUPATH_0010660]`                         +
          # `Cumulative sum aatA-pos stools [EUPATH_0010664]`                         +
          # `Cumulative sum bfpA-pos stools [EUPATH_0010668]`                         +
          # `Cumulative sum eae-pos stools [EUPATH_0010672]`                          +
          # `Cumulative sum ETEC LT-pos stools [EUPATH_0010680]`                      +
          # `Cumulative sum ETEC ST-pos stools [EUPATH_0010684]`                      +
          # `Cumulative sum stx1-pos stools [EUPATH_0010688]`                         +
          # `Cumulative sum stx2-pos stools [EUPATH_0010692]`                         +
          # `Cumulative sum Entamoeba histolytica-pos stools [EUPATH_0010696]`        +
          # `Cumulative sum EIEC-pos stools [EUPATH_0010700]`                         +
          # `Cumulative sum Endolimax nana-pos stools [EUPATH_0010704]`               +
          # `Cumulative sum EPEC-pos stools [EUPATH_0010708]`                         +
          # `Cumulative sum ETEC-pos stools [EUPATH_0010712]`                         +
          # # `Cumulative sum Enterobius vermicularis-pos stools [EUPATH_0010716]`      +
          # `Cumulative sum Giardia-pos stools [EUPATH_0010723]`                      +
          # `Cumulative sum Hymenolepis diminuta-pos stools [EUPATH_0010727]`         +
          # `Cumulative sum Hymenolepis nana-pos stools [EUPATH_0010731]`             +
          # `Cumulative sum Hookworm-pos stools [EUPATH_0010735]`                     +
          # `Cumulative sum Iodamoeba butschlii-pos stools [EUPATH_0010739]`          +
          # `Cumulative sum Isospora-pos stools [EUPATH_0010743]`                     +
          # `Cumulative sum ETEC LT-pos ST-neg stools [EUPATH_0010747]`               +
          # `Cumulative sum Cryptosporidium-pos stools, by microscopy [EUPATH_0010751]`+
          # `Cumulative sum Entamoeba coli-pos stools [EUPATH_0010755]`               +
          # `Cumulative sum Norovirus-pos stools [EUPATH_0010768]`                    +
          # `Cumulative sum Norovirus GI-pos stools [EUPATH_0010772]`                 +
          # `Cumulative sum Norovirus GII-pos stools [EUPATH_0010776]`                +
          # `Cumulative sum Plesiomonas shigelloides-pos stools [EUPATH_0010790]`     +
          # `Cumulative sum Rotavirus-pos stools [EUPATH_0010794]`                    +
          # `Cumulative sum Salmonella-pos stools [EUPATH_0010798]`                   +
          # # `Cumulative sum Schistosoma-pos stools [EUPATH_0010802]`                  +
          # `Cumulative sum Shigella-pos stools [EUPATH_0010806]`                     +
          # `Cumulative sum Strongyloides stercoralis-pos stools [EUPATH_0010810]`    +
          # `Cumulative sum STEC-pos stools [EUPATH_0010818]`                         +
          # # `Cumulative sum Taenia-pos stools [EUPATH_0010822]`                       +
          # `Cumulative sum Vibrio-pos stools [EUPATH_0010826]`                       +
          # `Cumulative sum Yersinia enterocolitica-pos stools [EUPATH_0010830]`      +
          factor(`Stool blood present [EUPATH_0015217]`)                                    +
          # factor(`Study-defined diarrhea [EUPATH_0000665]`)                                +
          `Cumulative diarrheal episode count [EUPATH_0010478]`                     +
          `Cumulative days within diarrheal episodes [EUPATH_0010480]`              +
          factor(`ALRI episode status [EUPATH_0010507]`)                                    +
          `Cumulative ALRI episode count [EUPATH_0010516]`                          +
          # factor(`Moderate-to-severe diarrhea by GEMS criteria [EUPATH_0010524]`)           +
          # factor(`Any illness, caregiver report [EUPATH_0010525]`)                          +
          factor(`Activity level, caregiver report [EUPATH_0010526]`)                       +
          factor(`Appetite, caregiver report [EUPATH_0010527]`)                             +
          factor(`Vomiting, caregiver report [EUPATH_0010528]`)                           +
          factor(`Ear pain or pulling, caregiver report [EUPATH_0010529]`)                  +
          factor(`Use of antibiotics, caregiver report [EUPATH_0010530]`)                   +
          `Loose stools in day, caregiver report [EUPATH_0010533]`                  +
          factor(`Blood in stool, caregiver report [EUPATH_0010534]`)                       +
          factor(`ORT administered, caregiver report [EUPATH_0010536]`)                     +
          factor(`Cough, caregiver report [EUPATH_0010538]`)                                +
          factor(`Shortness of breath, caregiver report [EUPATH_0010539]`)                  +
          factor(`Fever, caregiver report [EUPATH_0010544]`)                                +
          factor(`Hospitalized [EUPATH_0010995]`)                                           +
          # `Cumulative days exclusively breastfed [EUPATH_0011014]`                  +
          # `Cumulative days not breastfed [EUPATH_0011015]`                          +
          # `Cumulative days partially breastfed [EUPATH_0011017]`                    +
          # `Cumulative days predominantly breastfed [EUPATH_0011019]`                +
          factor(`Dairy reported at last visit [EUPATH_0011020]`)                           +
          # factor(`Exclusively breastfed at last visit [EUPATH_0011023]`)                    +
          # factor(`Not breastfed at last visit [EUPATH_0011041]`)                            +
          # factor(`Partially breastfed at last visit [EUPATH_0011043]`)                      +
          # factor(`Predominantly breastfed at last visit [EUPATH_0011046]`)                 +
          # factor(`Breastfeeding status [EUPATH_0011048]`)                                   +
          # `Animal milk or formula feedings yesterday [EUPATH_0011641]`              +
          # factor(`Active breastfeeding [EUPATH_0011645]`)                                   +
          # `Breastfeedings yesterday, daytime [EUPATH_0011646]`                      +
          # `Breastfeedings yesterday, nighttime [EUPATH_0011647]`                    +
          # `Formula feedings yesterday, daytime [EUPATH_0011656]`                    +
          # `Formula feedings yesterday, nighttime [EUPATH_0011657]`                  +
          # factor(`Infant formula [FOODON_03303130]`)                                        +
          `Age 1st animal milk or formula given (days) [EUPATH_0011030]`            +
          # factor(`Breastfed within 1st 24hrs [EUPATH_0011009]`)                             +
          factor(`Time between childbirth and 1st breastfeeding [EUPATH_0011010]`)          +
          factor(`Fed colostrum [EUPATH_0011011]`)                                          +
          factor(`Prelacteal feeding [EUPATH_0011012]`)                                     +
          `Age 1st animal milk or solids given (days) [EUPATH_0011033]`             +
          `Age 1st solid food given (days) [EUPATH_0011036]`                        +
          `Age last exclusively breastfed (days) [EUPATH_0011049]`                  +
          factor(`Sex [PATO_0000047]`)                                                      +
          factor(`Bed [ENVO_00000501]`)                                                     +
          # factor(`Drinking water source [ENVO_00003064]`)                                   +
          factor(`Television [ENVO_01000579]`)                                              +
          factor(`Refrigerator [ENVO_01000583]`)                                            +
          factor(`Table [ENVO_01000584]`)                                                   +
          factor(`Chair [ENVO_01000586]`)                                                   +
          factor(`Roof material [EUPATH_0000003]`)                                          +
          factor(`Floor material [EUPATH_0000006]`)                                         +
          factor(`Wall material [EUPATH_0000009]`)                                          +
          factor(`Bank account [EUPATH_0000167]`)                                           +
          `Persons sleeping in dwelling [EUPATH_0000714]`                           +
          `Family income (local currency) [EUPATH_0000727]`                         +
          factor(`Went 24hrs without food last month [EUPATH_0011135]`)                     +
          factor(`Ate foods not wanted last month [EUPATH_0011136]`)                        +
          factor(`Not able to eat preferred foods last month [EUPATH_0011137]`)             +
          factor(`Ate fewer meals due to lack of food last month [EUPATH_0011138]`)         +
          factor(`Went to sleep hungry last month [EUPATH_0011139]`)                        +
          factor(`Had to eat a limited variety of foods last month [EUPATH_0011141]`)       +
          factor(`No food of any kind last month [EUPATH_0011142]`)                         +
          factor(`Worried that there was not enough food last month [EUPATH_0011143]`)      +
          factor(`Ate smaller meal than needed last month [EUPATH_0011144]`)                +
          factor(`Household Food Insecurity Access Scale (HFIAS) [EUPATH_0011145]`)         +
          # factor(`Toilet or latrine type [EUPATH_0011744]`)                                 +
          factor(`Urban or rural site [EUPATH_0011928]`)                                    +
          factor(`specific_partition`) + 
          `Alpha-1-antitrypsin (mg/g) [EUPATH_0011276]` + 
          `Myeloperoxidase (ng/mL) [EUPATH_0011277]` + 
          `Neopterin (nmol/L) [EUPATH_0011278]` + 
          factor(`Antibiotics, caregiver report [EUPATH_0000582]`) + 
          `Calcium phytate to zinc ratio [EUPATH_0011054]` + 
          `Calories from carbohydrates (%) [EUPATH_0011064]` + 
          `Calories from fat (%) [EUPATH_0011065]` + 
          `Calories from protein (%) [EUPATH_0011066]` + 
          `Meal count during recall [EUPATH_0011067]` + 
          `Phytate to calcium ratio [EUPATH_0011068]` + 
          `Phytate to iron ratio [EUPATH_0011069]` + 
          `Phytate to zinc ratio [EUPATH_0011070]` + 
          `Dairy count [EUPATH_0011074]` + 
          `Dark green leafy vegetable count [EUPATH_0011075]` + 
          `Egg count [EUPATH_0011076]` + 
          `Fish or shellfish count [EUPATH_0011077]` + 
          `Legume count [EUPATH_0011078]` + 
          `Meat count [EUPATH_0011079]` + 
          `Other fruit or vegetable count [EUPATH_0011080]` + 
          `Organ meat count [EUPATH_0011081]` + 
          `Sweets count [EUPATH_0011082]` + 
          `Daily protein from animal sources (g) [EUPATH_0011083]` + 
          `Daily calcium (mg) [EUPATH_0011084]` + 
          `Daily carbohydrates (g) [EUPATH_0011085]` + 
          `Daily cholesterol (mg) [EUPATH_0011086]` + 
          `Coffee or tea count [EUPATH_0011087]` + 
          `Daily copper (mg) [EUPATH_0011088]` + 
          `Daily monounsaturated fat (g) [EUPATH_0011089]` + 
          `Daily polyunsaturated fat (g) [EUPATH_0011090]` + 
          `Daily saturated fat (g) [EUPATH_0011091]` + 
          `Daily fat (g) [EUPATH_0011092]` + 
          `Daily iron from all sources (mg) [EUPATH_0011093]` + 
          `Daily fiber (g) [EUPATH_0011094]` + 
          `Daily folate (ug) [EUPATH_0011095]` + 
          `Grain count [EUPATH_0011096]` + 
          `Daily potassium (mg) [EUPATH_0011097]` + 
          `Daily calories (Kcal) [EUPATH_0011098]` + 
          `Daily iron from meat, poultry, fish (mg) [EUPATH_0011099]` + 
          `Daily protein from meat, poultry, fish (g) [EUPATH_0011100]` + 
          `Daily magnesium (mg) [EUPATH_0011101]` + 
          `Daily manganese (mg) [EUPATH_0011102]` + 
          `Daily sodium (mg) [EUPATH_0011103]` + 
          `Daily niacin (mg) [EUPATH_0011104]` + 
          `Daily phosphorus (mg) [EUPATH_0011105]` + 
          `Daily pantothenic acid (mg) [EUPATH_0011106]` + 
          `Daily phytate (mg) [EUPATH_0011107]` + 
          `Daily protein from all sources (g) [EUPATH_0011108]` + 
          `Daily riboflavin (mg) [EUPATH_0011109]` + 
          `Roots or tubers count [EUPATH_0011110]` + 
          `Daily sugars (g) [EUPATH_0011111]` + 
          `Daily thiamin (mg) [EUPATH_0011112]` + 
          `Daily vitamin A (ug) [EUPATH_0011113]` + 
          `Daily vitamin B12 (ug) [EUPATH_0011114]` + 
          `Daily vitamin B6 (mg) [EUPATH_0011115]` + 
          `Daily vitamin C (mg) [EUPATH_0011116]` + 
          `Daily vitamin D (ug) [EUPATH_0011117]` + 
          `Daily vitamin E (mg) [EUPATH_0011118]` + 
          `Yellow fruits count [EUPATH_0011119]` + 
          `Yellow vegetables count [EUPATH_0011120]` + 
          `Daily zinc (mg) [EUPATH_0011121]` + 
          # factor(`Animal proteins (meat, eggs, fish) [EUPATH_0011623]`) + 
          # factor(`Animal proteins (meat, organ meat, fish) [EUPATH_0011627]`) + 
          # factor(`Animal proteins (including grains and roots) [EUPATH_0011628]`) + 
          # factor(`Complementary food introduction [EUPATH_0011629]`) + 
          # factor(`Dark green leafy vegetables, organ meat, any meat [EUPATH_0011630]`) + 
          # factor(`Organ meat, any meat [EUPATH_0011631]`) + 
          # factor(`Dark green leafy vegetables, organ meat, any meat, eggs [EUPATH_0011632]`) + 
          # factor(`Organ meat, any meat, eggs [EUPATH_0011633]`) + 
          # factor(`Dark green leafy vegetables, organ meat, any meat, fish [EUPATH_0011634]`) + 
          # factor(`Organ meat, any meat, fish [EUPATH_0011635]`) + 
          # factor(`Dark green leafy vegetables, organ meat, any meat, eggs, fish [EUPATH_0011636]`) + 
          # factor(`Organ meat, any meat, eggs, fish [EUPATH_0011637]`) + 
          # factor(`Min acceptable diet for non-breastfed children [EUPATH_0011639]`) + 
          # factor(`More than 1 animal milk or formula feeds for non-breastfed kids [EUPATH_0011642]`) + 
          # factor(`Legumes [EUPATH_0011644]`) + 
          # factor(`Commercially available foods [EUPATH_0011648]`) + 
          # factor(`Dairy [EUPATH_0011649]`) + 
          # factor(`Eggs [EUPATH_0011651]`) + 
          # `Feedings yesterday [EUPATH_0011652]` + 
          # factor(`Fish or shellfish [EUPATH_0011653]`) + 
          # factor(`Other fruits or vegetables [EUPATH_0011658]`) + 
          # factor(`Grains [EUPATH_0011659]`) + 
          # factor(`Fruit or vegetable juice [EUPATH_0011660]`) + 
          # factor(`Meat [EUPATH_0011661]`) + 
          # factor(`Animal milk [EUPATH_0011662]`) + 
          # `Animal milk feedings yesterday, daytime [EUPATH_0011663]` + 
          # `Animal milk feedings yesterday, nighttime [EUPATH_0011664]` + 
          # factor(`Organ meats [EUPATH_0011665]`) + 
          # factor(`Other liquids (soup, broth, sugar water, carbonated drinks) [EUPATH_0011666]`) + 
          # factor(`Roots or tubers [EUPATH_0011667]`) + 
          # factor(`Dark green leafy vegetables [EUPATH_0011668]`) + 
          # factor(`Sweets [EUPATH_0011669]`) + 
          # factor(`Coffee or tea [EUPATH_0011670]`) + 
          # factor(`Plain water [EUPATH_0011671]`) + 
          # factor(`Yellow fruits [EUPATH_0011672]`) + 
          # factor(`Yellow vegetables [EUPATH_0011673]`) + 
          # factor(`Vitamin A fruits or vegetables [EUPATH_0011677]`) + 
          # `Different food groups consumed [EUPATH_0011678]` + 
          factor(`Dehydration, caregiver report [HP_0001944]`) + 
          factor(`Separate kitchen [EUPATH_0011586]`) + 
          `Room count [EUPATH_0011593]` + 
          factor(`Income score [EUPATH_0011601]`) + 
          factor(`Drinking water score [EUPATH_0011603]`) + 
          `Mean people per room [EUPATH_0011604]` + 
          `Maternal education (years) [EUPATH_0011605]` + 
          factor(`Sanitation score [EUPATH_0011607]`) + 
          factor(`Fewer than 2 people per room [EUPATH_0011609]`) + 
          factor(`Sanitation and drinking water score [EUPATH_0011617]`) + 
          `WAMI index [EUPATH_0011620]` + 
          factor(breastfed_status_combined) +
          factor(improved_drinking_water) + 
          factor(improved_waste_disposal) + 
          factor(wet_season),
        data = df_env, family = binomial(link = "probit"),
    )
  )
}
