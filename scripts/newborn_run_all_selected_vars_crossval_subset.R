# Run all newborn with the selected variables, to be run after the crossval variable selection. 

# Make a cluster for parallel MCMCs
library(readr)
library(dplyr)
library(foreach)
library(doSNOW)
library(BayesComm)


# source(file.path("scripts", "pre_processing.R"), echo = F)
source(file.path("scripts", "constants.R"), echo = F)
source(file.path("scripts", "transferred_functions.R"), echo = F)

##### CONSTANTS #####
EXP_NAME <- file_pattern <- "newborn_crossval_subset"

n_iterations <-5e4 #Final: 5e4
n_burnin <- 1e4 #Final: 1e4
sample_size <- 1e3 #Final: 1e3
thinning <- n_iterations/sample_size

n_latent <- 2

nchains <- 4
# ncores <- nchains ## One core for each MCMC chains
ncores <- parallel::detectCores()
k_fold <- 10
#####

# Import the dataset, we'll cut out the seperate script since the data is already prepared. 

# Seperate into presence and absence data, and ENV variables. 

all_data <- read_csv(file.path("data", "newborn_selected_transformed_variables_crossval_subset.csv")) 

### CODE TO IMPORT ENV DATA FROM SELECTED VARIABLES HERE

###


PA_data <- all_data %>% select(one_of(get_pathogens(), "k_partition"))
ENV_data <- all_data %>% select(-one_of(get_pathogens()))

clust <- makeCluster(ncores)
registerDoSNOW(clust)
# registerDoParallel(clust)

if(!dir.exists(file.path("models", "crossval_model", EXP_NAME))){
  dir.create(file.path("models", "crossval_model", EXP_NAME))
}


# Calculate the DIC scores and save to file. The Saved DIC scores can prevent us from calculating it down the road. 

all_dic_scores <- foreach(k = seq(k_fold), .combine = 'rbind')%:% foreach(i = seq(nchains), .combine = 'rbind') %dopar% {
  library(dplyr)
  
  # Make a subdirectory for the crossval information
  cross_val_directory <- file.path("models", "crossval_model", EXP_NAME, paste("crossval", k, sep = "_"))
  
  # # Split the dataset
  if(!dir.exists(cross_val_directory)){
    dir.create(cross_val_directory)
  }
  # 
  # PA_k_fold_subset <- all_training_PA %>% slice(cross_val_splits[[k]])
  # env_k_fold_subset <- all_training_env %>% slice(cross_val_splits[[k]])

  
  PA_k_fold_subset <- PA_data %>% filter(k_partition == k) %>%  select(-one_of("k_partition")) %>% select(one_of(get_pathogens()))
  env_k_fold_subset <- ENV_data %>% filter(k_partition == k) %>% select(-one_of("k_partition"))
  
  local_dic_scores <- vector(mode = "numeric", length = 4)
  model_types <- c("full", "environment", "community", "null")
  
  # Run each of the four chains.
  all_local_models <- list()
  dic_scores <- list()
  # Full
  for(m in 1:length(model_types)){
    # fitted_bc_chain <- NA
    fitted_bc_chain <- BayesComm::BC(
      Y = as.matrix(PA_k_fold_subset),
      X = as.matrix(env_k_fold_subset),
      model = model_types[m],
      its = n_iterations + n_burnin,
      verbose = 1,
      thin = thinning,
      burn = n_burnin)
    # Save each model type to it's own folder with all the different chains. 4 items per folder.
    
    
    local_dic_scores[m] <- BayesComm::DIC(fitted_bc_chain)
    # Function for saving model data to file to check for convergence.
    # model_to_save <- fitted_bc_chain
    fitted_bc_chain$trace$z <- NULL
    
    model_subdirectory <- file.path(cross_val_directory, model_types[m])
    
    # Split the dataset
    if(!dir.exists(model_subdirectory)){
      dir.create(model_subdirectory)
    }
    
    saveRDS(fitted_bc_chain, file = file.path(model_subdirectory, paste(c("bayescomm_", model_types[m] , "_", i, ".rda"), collapse = "")))
    
    temp_data_frame <- data.frame(model_type = model_types, dic_scores = local_dic_scores)
    
    # print(nrow(temp_data_frame))
    # print(is.data.frame(temp_data_frame))
    
    rm(fitted_bc_chain)
    
    
  }
  temp_data_frame$crossval <- k
  temp_data_frame$chain <- i
  
  
  temp_data_frame
  
}

stopCluster(clust)

write.csv(all_dic_scores, file.path("models", "crossval_model", EXP_NAME, "dic_scores.csv"), row.names = F)
