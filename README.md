# pathogen_jsdm

Using the Joint Species Distribution Model to look at the correlation between pathogens in the presence of an environmental variables. Relies on The MAL-ED dataset. Originally started as a project for UVM CS387.  

## Data Origin 

Data originates from the [MAL-ED dataset](https://clinepidb.org/ce/app/record/dataset/DS_5c41b87221). Observations contain nutrition information from food surveys conducted every two weeks. Pathogen Presence/Absence was determined from qPCR data generated from stool samples collected from participants during their first year of age. Pathogen was determiend to be "present" with a qPCR Ct value < 35. Includes nutrition information, socioecominc factors, water and water sources.  


### Data Partitions: 
There's two models that we'll use, one at 0-9 months, and another at 9-15 months. This is because of infant diets, mostly breastfed before 9 months, so dietary information is useless.  

## Method: JSDM

The joint species distribution model estimates the tradeoff between the correlation due to the envirnment and the correlation due to a shared response due to a possible biotic relationship between the species. The package I'll use here comes from the [BayesComm](https://cran.r-project.org/web/packages/BayesComm/index.html) package. 

## Repo Organisation

There are three main directories: 
   * **notebooks**: All the the R markdown notebooks I use for testing the code and models in runtime as well as generatinv figures. 
   * **scripts**: function I use within the notebooks, as well as scripts for running all the MCMC sampling, best to use for running models in parallel. 
   * **models**: where I save models using RDA files. Files are large, so they are only saved locally, not on the repo. 
   * **figures**: All of the plots used in the paper. 
   * **paper**: Final paper for DS2 submission. 
   * **data**: MAL-ED is an access by request dataset, so to honor that agreement, data that's used in this analysis is not on this repo while it's public. Once we have permission to store publicly or mask the data it will be available here. Right now it's available by request. 

  
