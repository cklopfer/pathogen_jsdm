# Build a test function to make sure the the env coefficient code is working 


# 5 Species 
# 2 covariates 

set.seed(10)
n_species <- 5
n_covariates <- 2
n_sites <- 200

# Fake Design Matrix
fake_data <- cbind(
  matrix(data = sample(2, n_covariates * n_sites, replace = T) - 1, ncol = n_covariates), 
  rep(1, n_sites))

fake_coef <- matrix(data = rnorm((n_covariates + 1) * n_species), ncol = n_species)

# Introduce shared reponse
for(c in 1:ncol(fake_coef)){
  if(c == 4 | c == 5){
    fake_coef[,c] <- fake_coef[,1] + rnorm(n_covariates + 1, 0, 0.05)
  }else if (c == 2 | c == 3){
    fake_coef[,c] <- fake_coef[,c] * 1e-6
  }
}

R <- matrix(data = 0.0001, nrow = n_species, ncol = n_species)

# Introduce the residual correaltion 
for(r in 1:nrow(R)){
  for(c in 1:ncol(R)){
    if(r == 2){
      R[r, 3] <- 0.8
    }else if(r == 3){
      R[r, 2] <- 0.8
    }
  }
}

diag(R) <- 1

e <- mvtnorm::rmvnorm(n_sites, sigma = R)

z = (fake_data %*% fake_coef) + e

fake_PA = (z > 0) * 1